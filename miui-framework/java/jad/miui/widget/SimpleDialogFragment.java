// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.widget;

import android.app.*;
import android.content.DialogInterface;
import android.os.Bundle;

public class SimpleDialogFragment extends DialogFragment {
    public static final class AlertDialogFragmentBuilder {

        public SimpleDialogFragment create() {
            if(mCreated) {
                throw new IllegalStateException("dialog has been created");
            } else {
                mCreated = true;
                SimpleDialogFragment simpledialogfragment = new SimpleDialogFragment();
                Bundle bundle = new Bundle();
                bundle.putString("title", mTitle);
                bundle.putString("msg_res_id", mMessage);
                bundle.putBoolean("cancelable", mCancelable);
                bundle.putInt("type", mType);
                simpledialogfragment.setArguments(bundle);
                return simpledialogfragment;
            }
        }

        public AlertDialogFragmentBuilder setCancelable(boolean flag) {
            mCancelable = flag;
            return this;
        }

        public AlertDialogFragmentBuilder setMessage(String s) {
            mMessage = s;
            return this;
        }

        public AlertDialogFragmentBuilder setTitle(String s) {
            mTitle = s;
            return this;
        }

        private boolean mCancelable;
        private boolean mCreated;
        private String mMessage;
        private String mTitle;
        private int mType;

        public AlertDialogFragmentBuilder(int i) {
            mCancelable = true;
            mType = i;
        }
    }


    public SimpleDialogFragment() {
        mCancelable = true;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle bundle1 = getArguments();
        if(bundle1 == null) {
            throw new IllegalStateException("no argument");
        } else {
            mType = bundle1.getInt("type");
            mMessage = bundle1.getString("msg_res_id");
            mTitle = bundle1.getString("title");
            mCancelable = bundle1.getBoolean("cancelable", true);
            return;
        }
    }

    public Dialog onCreateDialog(Bundle bundle) {
        mType;
        JVM INSTR tableswitch 1 2: default 28
    //                   1 58
    //                   2 138;
           goto _L1 _L2 _L3
_L1:
        throw new IllegalStateException((new StringBuilder()).append("unknown dialog type:").append(mType).toString());
_L2:
        Object obj;
        android.app.AlertDialog.Builder builder = (new android.app.AlertDialog.Builder(getActivity())).setMessage(mMessage).setCancelable(mCancelable).setTitle(mTitle);
        if(mPositiveButtonTextRes > 0)
            builder.setPositiveButton(mPositiveButtonTextRes, mPositiveButtonClickListener);
        if(mNegativeButtonTextRes > 0)
            builder.setNegativeButton(mNegativeButtonTextRes, mNegativeButtonClickListener);
        obj = builder.create();
_L5:
        return ((Dialog) (obj));
_L3:
        obj = new ProgressDialog(getActivity());
        ((ProgressDialog) (obj)).setMessage(mMessage);
        ((ProgressDialog) (obj)).setCancelable(mCancelable);
        if(true) goto _L5; else goto _L4
_L4:
    }

    public void onDismiss(DialogInterface dialoginterface) {
        super.onDismiss(dialoginterface);
        if(mOnDismissListener != null)
            mOnDismissListener.onDismiss(dialoginterface);
    }

    public void setNegativeButton(int i, android.content.DialogInterface.OnClickListener onclicklistener) {
        mNegativeButtonTextRes = i;
        mNegativeButtonClickListener = onclicklistener;
    }

    public void setOnDismissListener(android.content.DialogInterface.OnDismissListener ondismisslistener) {
        mOnDismissListener = ondismisslistener;
    }

    public void setPositiveButton(int i, android.content.DialogInterface.OnClickListener onclicklistener) {
        mPositiveButtonTextRes = i;
        mPositiveButtonClickListener = onclicklistener;
    }

    public static final String ARG_CANCELABLE = "cancelable";
    public static final String ARG_MESSAGE = "msg_res_id";
    public static final String ARG_TITLE = "title";
    public static final String ARG_TYPE = "type";
    public static final int TYPE_ALERT = 1;
    public static final int TYPE_PROGRESS = 2;
    private boolean mCancelable;
    private String mMessage;
    private android.content.DialogInterface.OnClickListener mNegativeButtonClickListener;
    private int mNegativeButtonTextRes;
    private android.content.DialogInterface.OnDismissListener mOnDismissListener;
    private android.content.DialogInterface.OnClickListener mPositiveButtonClickListener;
    private int mPositiveButtonTextRes;
    private String mTitle;
    private int mType;
}
