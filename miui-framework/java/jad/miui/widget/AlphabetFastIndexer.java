// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.*;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.*;
import java.lang.ref.WeakReference;
import java.util.Arrays;

public class AlphabetFastIndexer extends ImageView {
    private static class OnScrollerDecorator
        implements android.widget.AbsListView.OnScrollListener {

        public void onScroll(AbsListView abslistview, int i, int j, int k) {
            AlphabetFastIndexer alphabetfastindexer = (AlphabetFastIndexer)mIndexerRef.get();
            if(alphabetfastindexer != null)
                alphabetfastindexer.refreshMask();
            if(mListener != null)
                mListener.onScroll(abslistview, i, j, k);
        }

        public void onScrollStateChanged(AbsListView abslistview, int i) {
            AlphabetFastIndexer alphabetfastindexer = (AlphabetFastIndexer)mIndexerRef.get();
            if(alphabetfastindexer != null)
                alphabetfastindexer.mListScrollState = i;
            if(mListener != null)
                mListener.onScrollStateChanged(abslistview, i);
        }

        private final WeakReference mIndexerRef;
        private final android.widget.AbsListView.OnScrollListener mListener;

        public OnScrollerDecorator(AlphabetFastIndexer alphabetfastindexer, android.widget.AbsListView.OnScrollListener onscrolllistener) {
            mIndexerRef = new WeakReference(alphabetfastindexer);
            mListener = onscrolllistener;
        }
    }

    public static interface AlphabetPattern {

        public abstract void draw(AlphabetFastIndexer alphabetfastindexer, Canvas canvas, Rect rect, int i, int j);
    }


    public AlphabetFastIndexer(Context context, AttributeSet attributeset) {
        this(context, attributeset, 0x60d002b);
    }

    public AlphabetFastIndexer(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
        mTextBounds = new Rect();
        mListScrollState = 0;
        mState = 0;
        Resources resources = context.getResources();
        TypedArray typedarray = context.obtainStyledAttributes(attributeset, com.miui.internal.R.styleable.AlphabetFastIndexer, i, 0x60d002b);
        mNormalColor = typedarray.getColor(2, resources.getColor(0x6070014));
        mHighlightColor = typedarray.getColor(3, resources.getColor(0x607001c));
        CharSequence acharsequence[] = typedarray.getTextArray(0);
        if(acharsequence != null) {
            mAlphabetTable = new String[acharsequence.length];
            int j = acharsequence.length;
            int k = 0;
            int i1;
            for(int l = 0; k < j; l = i1) {
                CharSequence charsequence = acharsequence[k];
                String as[] = mAlphabetTable;
                i1 = l + 1;
                as[l] = charsequence.toString();
                k++;
            }

        } else {
            mAlphabetTable = resources.getStringArray(0x6060003);
        }
        mOverlayLeftMargin = typedarray.getDimensionPixelOffset(6, resources.getDimensionPixelOffset(0x60a000a));
        mOverlayTopMargin = typedarray.getDimensionPixelOffset(7, resources.getDimensionPixelOffset(0x60a0008));
        mOverlayTextSize = typedarray.getDimensionPixelSize(8, resources.getDimensionPixelSize(0x60a0007));
        mOverlayTextColor = typedarray.getColor(9, resources.getColor(0x6070003));
        mOverlayBackground = typedarray.getDrawable(5);
        if(mOverlayBackground == null)
            mOverlayBackground = resources.getDrawable(0x6020003);
        TextPaint textpaint = new TextPaint();
        textpaint.setAntiAlias(true);
        textpaint.setTextAlign(android.graphics.Paint.Align.CENTER);
        textpaint.setTypeface(Typeface.DEFAULT_BOLD);
        textpaint.setTextSize(typedarray.getDimension(1, resources.getDimension(0x60a0006)));
        mPaint = textpaint;
        setBackgroundDrawable(typedarray.getDrawable(4));
        typedarray.recycle();
        mVerticalPosition = 5;
    }

    private void drawThumbInternal(CharSequence charsequence) {
        if(mListView != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        mOverlay.setText(charsequence);
        if(getVisibility() == 0) {
            mOverlay.setVisibility(0);
            mHandler.removeMessages(1);
            Message message = mHandler.obtainMessage(1);
            mHandler.sendMessageDelayed(message, 1500L);
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    private int getListOffset() {
        int i;
        if(mListView instanceof ListView)
            i = ((ListView)mListView).getHeaderViewsCount();
        else
            i = 0;
        return i;
    }

    private int getPostion(float f, SectionIndexer sectionindexer) {
        int i;
        Object aobj[];
        i = -1;
        aobj = sectionindexer.getSections();
        if(aobj != null) goto _L2; else goto _L1
_L1:
        return i;
_L2:
        int j = getPaddingTop();
        int k = getPaddingBottom();
        int l = getHeight() - j - k;
        if(l > 0) {
            int i1 = (int)(((f - (float)j) / (float)l) * (float)mAlphabetTable.length);
            if(i1 >= 0)
                if(i1 >= mAlphabetTable.length) {
                    i = aobj.length;
                } else {
                    i = Arrays.binarySearch(aobj, mAlphabetTable[i1]);
                    if(i < 0)
                        i = -2 + -i;
                    if(i < 0)
                        i = 0;
                }
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    private SectionIndexer getSectionIndexer() {
        if(mListView != null) goto _L2; else goto _L1
_L1:
        SectionIndexer sectionindexer = null;
_L4:
        return sectionindexer;
_L2:
        sectionindexer = null;
        android.widget.Adapter adapter = mListView.getAdapter();
        if(adapter instanceof SectionIndexer)
            sectionindexer = (SectionIndexer)adapter;
        else
        if(adapter instanceof HeaderViewListAdapter) {
            HeaderViewListAdapter headerviewlistadapter = (HeaderViewListAdapter)adapter;
            if(headerviewlistadapter.getWrappedAdapter() instanceof SectionIndexer)
                sectionindexer = (SectionIndexer)headerviewlistadapter.getWrappedAdapter();
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    private void scrollTo(SectionIndexer sectionindexer, int i) {
        int j;
        int k;
        float f;
        Object aobj[];
        j = mListView.getCount();
        k = getListOffset();
        f = 1.0F / (float)j / 8F;
        aobj = sectionindexer.getSections();
        if(aobj == null || aobj.length <= 1) goto _L2; else goto _L1
_L1:
        int i1;
        int j1;
        int k1;
        int l1;
        int i2;
        int j2;
        int k2;
        int l2;
        int i3;
        j1 = aobj.length;
        k1 = i;
        if(k1 >= j1)
            k1 = j1 - 1;
        l1 = k1;
        i1 = k1;
        i2 = sectionindexer.getPositionForSection(k1);
        j2 = j;
        k2 = i2;
        l2 = k1;
        i3 = k1 + 1;
        int j3 = j1 - 1;
        if(k1 < j3)
            j2 = sectionindexer.getPositionForSection(k1 + 1);
        if(j2 != i2) goto _L4; else goto _L3
_L3:
        if(k1 > 0) {
            k1--;
            k2 = sectionindexer.getPositionForSection(k1);
            int k3;
            if(k2 != i2) {
                l2 = k1;
                i1 = k1;
            } else {
                if(k1 != 0)
                    continue; /* Loop/switch isn't completed */
                i1 = 0;
            }
        }
_L4:
        for(k3 = i3 + 1; k3 < j1 && sectionindexer.getPositionForSection(k3) == j2;) {
            k3++;
            i3++;
        }

        break; /* Loop/switch isn't completed */
        if(true) goto _L3; else goto _L5
_L5:
        float f1 = (float)l2 / (float)j1;
        float f2 = (float)i3 / (float)j1;
        float f3 = (float)i / (float)j1;
        String s;
        int l3;
        if(l2 == l1 && f3 - f1 < f)
            l3 = k2;
        else
            l3 = k2 + Math.round(((float)(j2 - k2) * (f3 - f1)) / (f2 - f1));
        if(l3 > j - 1)
            l3 = j - 1;
        if(mListView instanceof ExpandableListView) {
            ExpandableListView expandablelistview1 = (ExpandableListView)mListView;
            expandablelistview1.setSelectionFromTop(expandablelistview1.getFlatListPosition(ExpandableListView.getPackedPositionForGroup(l3 + k)), 0);
        } else
        if(mListView instanceof ListView)
            ((ListView)mListView).setSelectionFromTop(l3 + k, 0);
        else
            mListView.setSelection(l3 + k);
_L7:
        if(i1 >= 0) {
            s = aobj[i1].toString();
            if(!TextUtils.isEmpty(s))
                drawThumbInternal(s.subSequence(0, 1));
        }
        return;
_L2:
        int l;
        l = Math.round(i * j);
        if(!(mListView instanceof ExpandableListView))
            break; /* Loop/switch isn't completed */
        ExpandableListView expandablelistview = (ExpandableListView)mListView;
        expandablelistview.setSelectionFromTop(expandablelistview.getFlatListPosition(ExpandableListView.getPackedPositionForGroup(l + k)), 0);
_L8:
        i1 = -1;
        if(true) goto _L7; else goto _L6
_L6:
        if(mListView instanceof ListView)
            ((ListView)mListView).setSelectionFromTop(l + k, 0);
        else
            mListView.setSelection(l + k);
          goto _L8
        if(true) goto _L7; else goto _L9
_L9:
    }

    public void attatch(AdapterView adapterview) {
        if(mListView != adapterview) goto _L2; else goto _L1
_L1:
        return;
_L2:
        detach();
        if(adapterview != null) {
            mLastAlphabetIndex = -1;
            mListView = adapterview;
            Context context = getContext();
            FrameLayout framelayout = (FrameLayout)getParent();
            mOverlay = new TextView(context);
            android.widget.FrameLayout.LayoutParams layoutparams = new android.widget.FrameLayout.LayoutParams(-2, -2, 17);
            layoutparams.leftMargin = mOverlayLeftMargin;
            layoutparams.topMargin = mOverlayTopMargin;
            mOverlay.setLayoutParams(layoutparams);
            mOverlay.measure(android.view.View.MeasureSpec.makeMeasureSpec(0, 0), android.view.View.MeasureSpec.makeMeasureSpec(0, 0));
            mOverlay.setBackgroundDrawable(mOverlayBackground);
            mOverlay.setGravity(17);
            mOverlay.setTextSize(mOverlayTextSize);
            mOverlay.setTextColor(mOverlayTextColor);
            mOverlay.setVisibility(8);
            framelayout.addView(mOverlay);
            android.widget.FrameLayout.LayoutParams layoutparams1 = (android.widget.FrameLayout.LayoutParams)getLayoutParams();
            layoutparams1.gravity = 0x30 | mVerticalPosition;
            setLayoutParams(layoutparams1);
            refreshMask();
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    public android.widget.AbsListView.OnScrollListener decorateScrollListener(android.widget.AbsListView.OnScrollListener onscrolllistener) {
        return new OnScrollerDecorator(this, onscrolllistener);
    }

    public void detach() {
        if(mListView != null) {
            stop(0);
            ((FrameLayout)getParent()).removeView(mOverlay);
            setVisibility(8);
            mListView = null;
        }
    }

    public void drawThumb(CharSequence charsequence) {
        if(mState == 0 && mListScrollState == 2)
            drawThumbInternal(charsequence);
    }

    public int getIndexerIntrinsicWidth() {
        Drawable drawable = getBackground();
        int i;
        if(drawable != null)
            i = drawable.getIntrinsicWidth();
        else
            i = 0;
        return i;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int i = getPaddingTop();
        int j = getHeight() - i - getPaddingBottom();
        if(j > 0) {
            TextPaint textpaint = mPaint;
            Rect rect = mTextBounds;
            String as[] = mAlphabetTable;
            float f = (float)j / (float)mAlphabetTable.length;
            float f1 = (float)getWidth() / 2.0F;
            int k = 0;
            while(k < as.length)  {
                String s;
                float f2;
                if(k != mLastAlphabetIndex)
                    textpaint.setColor(mNormalColor);
                else
                    textpaint.setColor(mHighlightColor);
                s = as[k];
                textpaint.getTextBounds(s, 0, s.length(), rect);
                f2 = f * (float)k + (float)i + (f - (float)(rect.top - rect.bottom)) / 2.0F;
                canvas.drawText(s, 0, s.length(), f1, f2, textpaint);
                if(mAlphabetPattern != null)
                    mAlphabetPattern.draw(this, canvas, rect, k, mLastAlphabetIndex);
                k++;
            }
        }
    }

    public void onSizeChanged(int i, int j, int k, int l) {
        super.onSizeChanged(i, j, k, l);
        mLastAlphabetIndex = -1;
        post(mRefreshMaskRunnable);
    }

    public boolean onTouchEvent(MotionEvent motionevent) {
        boolean flag = false;
        if(mListView != null) goto _L2; else goto _L1
_L1:
        stop(0);
_L4:
        return flag;
_L2:
        SectionIndexer sectionindexer;
        sectionindexer = getSectionIndexer();
        if(sectionindexer == null) {
            stop(0);
            continue; /* Loop/switch isn't completed */
        }
        switch(0xff & motionevent.getAction()) {
        case 1: // '\001'
        default:
            stop(1500);
            break;

        case 0: // '\0'
            break; /* Loop/switch isn't completed */

        case 2: // '\002'
            break MISSING_BLOCK_LABEL_90;
        }
_L5:
        flag = true;
        if(true) goto _L4; else goto _L3
_L3:
        mState = 1;
        setPressed(true);
        int i = getPostion(motionevent.getY(), sectionindexer);
        if(i < 0)
            mListView.setSelection(0);
        else
            scrollTo(sectionindexer, i);
          goto _L5
    }

    public void refreshMask() {
        if(mListView != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        int i = 0;
        SectionIndexer sectionindexer = getSectionIndexer();
        if(sectionindexer != null) {
            int j = sectionindexer.getSectionForPosition(mListView.getFirstVisiblePosition() - getListOffset());
            if(j != -1) {
                String s = (String)sectionindexer.getSections()[j];
                if(!TextUtils.isEmpty(s))
                    i = Arrays.binarySearch(mAlphabetTable, s);
            }
        }
        if(mLastAlphabetIndex != i) {
            mLastAlphabetIndex = i;
            invalidate();
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    public void setAlphabetStyle(AlphabetPattern alphabetpattern) {
        mAlphabetPattern = alphabetpattern;
    }

    public void setOverlayOffset(int i, int j) {
        mOverlayLeftMargin = i;
        mOverlayTopMargin = j;
    }

    public void setVerticalPosition(boolean flag) {
        int i;
        if(flag)
            i = 5;
        else
            i = 3;
        mVerticalPosition = i;
    }

    void stop(int i) {
        setPressed(false);
        mState = 0;
        mHandler.removeMessages(1);
        if(i <= 0) {
            if(mOverlay != null)
                mOverlay.setVisibility(8);
        } else {
            Message message = mHandler.obtainMessage(1);
            mHandler.sendMessageDelayed(message, i);
        }
    }

    private static final int FADE_DELAYED = 1500;
    private static final int MSG_FADE = 1;
    public static final int STATE_DRAGGING = 1;
    public static final int STATE_NONE;
    private AlphabetPattern mAlphabetPattern;
    private String mAlphabetTable[];
    private Handler mHandler = new Handler() {

        public void handleMessage(Message message) {
            message.what;
            JVM INSTR tableswitch 1 1: default 24
        //                       1 25;
               goto _L1 _L2
_L1:
            return;
_L2:
            if(mOverlay != null)
                mOverlay.setVisibility(8);
            if(true) goto _L1; else goto _L3
_L3:
        }

        final AlphabetFastIndexer this$0;

             {
                this$0 = AlphabetFastIndexer.this;
                super();
            }
    };
    private int mHighlightColor;
    private int mLastAlphabetIndex;
    int mListScrollState;
    private AdapterView mListView;
    private int mNormalColor;
    private TextView mOverlay;
    private Drawable mOverlayBackground;
    private int mOverlayLeftMargin;
    private int mOverlayTextColor;
    private int mOverlayTextSize;
    private int mOverlayTopMargin;
    private final TextPaint mPaint;
    private Runnable mRefreshMaskRunnable = new Runnable() {

        public void run() {
            refreshMask();
        }

        final AlphabetFastIndexer this$0;

             {
                this$0 = AlphabetFastIndexer.this;
                super();
            }
    };
    private int mState;
    private final Rect mTextBounds;
    private int mVerticalPosition;

}
