// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.*;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import java.util.HashMap;

public class ImageTextView extends LinearLayout {

    public ImageTextView(Context context) {
        this(context, null);
    }

    public ImageTextView(Context context, AttributeSet attributeset) {
        this(context, attributeset, 0);
    }

    public ImageTextView(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
        VIEW_PARAMS = new android.widget.LinearLayout.LayoutParams(-2, -2);
        int ai[] = new int[2];
        ai[0] = 0x10100b2;
        ai[1] = 0x10101f8;
        ImageTextView_Styleable = ai;
        ENTRIES_INDEX = 0;
        ENTRYVALUES_INDEX = 1;
        setOrientation(0);
        TypedArray typedarray = context.obtainStyledAttributes(attributeset, ImageTextView_Styleable, i, 0);
        CharSequence acharsequence[] = typedarray.getTextArray(0);
        Integer ainteger[] = getDrawableIds(typedarray);
        if(acharsequence != null && ainteger != null)
            if(acharsequence.length == ainteger.length) {
                mCharMap = new HashMap();
                for(int j = 0; j < acharsequence.length; j++)
                    mCharMap.put(Character.valueOf(acharsequence[j].charAt(0)), ainteger[j]);

            } else {
                Log.e("ImageTextView", "keys and values not matched");
            }
    }

    private Integer[] getDrawableIds(TypedArray typedarray) {
        TypedValue typedvalue = new TypedValue();
        Integer ainteger[] = null;
        if(typedarray.getValue(1, typedvalue)) {
            TypedArray typedarray1 = getContext().getResources().obtainTypedArray(typedvalue.resourceId);
            ainteger = new Integer[typedarray1.length()];
            for(int i = 0; i < typedarray1.length(); i++)
                ainteger[i] = Integer.valueOf(typedarray1.peekValue(i).resourceId);

        }
        return ainteger;
    }

    public void setCharacterMap(HashMap hashmap) {
        mCharMap = hashmap;
    }

    public void setText(CharSequence charsequence) {
        if(mCharMap != null) {
            int i = 0;
            while(i < charsequence.length())  {
                Object obj;
                Integer integer;
                if(i < getChildCount()) {
                    obj = getChildAt(i);
                } else {
                    obj = new ImageView(mContext);
                    ((ImageView) (obj)).setLayoutParams(VIEW_PARAMS);
                    addView(((View) (obj)));
                }
                integer = (Integer)mCharMap.get(Character.valueOf(charsequence.charAt(i)));
                if(integer != null)
                    ((View) (obj)).setBackground(getResources().getDrawable(integer.intValue()));
                else
                    Log.e("ImageTextView", (new StringBuilder()).append("No drawable id maps ").append(charsequence.charAt(i)).toString());
                i++;
            }
            int j = getChildCount();
            while(j > charsequence.length())  {
                removeViewAt(j - 1);
                j--;
            }
        }
    }

    private static final String TAG = "ImageTextView";
    private final int ENTRIES_INDEX;
    private final int ENTRYVALUES_INDEX;
    private final int ImageTextView_Styleable[];
    private final android.widget.LinearLayout.LayoutParams VIEW_PARAMS;
    private HashMap mCharMap;
}
