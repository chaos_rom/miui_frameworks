// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.widget;

import android.content.Context;
import android.graphics.drawable.AnimatedRotateDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

public class AnimatedImageView extends ImageView {

    public AnimatedImageView(Context context) {
        super(context);
    }

    public AnimatedImageView(Context context, AttributeSet attributeset) {
        super(context, attributeset);
    }

    private void updateAnimating() {
        if(mDrawable != null)
            if(isShown() && mAnimating)
                mDrawable.start();
            else
                mDrawable.stop();
    }

    private void updateDrawable() {
        if(isShown() && mDrawable != null)
            mDrawable.stop();
        Drawable drawable = getDrawable();
        if(drawable instanceof AnimatedRotateDrawable) {
            mDrawable = (AnimatedRotateDrawable)drawable;
            mDrawable.setFramesCount(56);
            mDrawable.setFramesDuration(32);
            if(isShown() && mAnimating)
                mDrawable.start();
        } else {
            mDrawable = null;
        }
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        updateAnimating();
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        updateAnimating();
    }

    protected void onVisibilityChanged(View view, int i) {
        super.onVisibilityChanged(view, i);
        updateAnimating();
    }

    public void setAnimating(boolean flag) {
        mAnimating = flag;
        updateAnimating();
    }

    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        updateDrawable();
    }

    public void setImageResource(int i) {
        super.setImageResource(i);
        updateDrawable();
    }

    private boolean mAnimating;
    private AnimatedRotateDrawable mDrawable;
}
