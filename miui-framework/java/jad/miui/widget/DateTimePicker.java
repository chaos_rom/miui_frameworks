// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.widget;

import android.content.Context;
import android.text.format.DateFormat;
import android.widget.FrameLayout;
import android.widget.NumberPicker;
import java.text.DateFormatSymbols;
import java.util.Calendar;

public class DateTimePicker extends FrameLayout {
    public static interface OnDateTimeChangedListener {

        public abstract void onDateTimeChanged(DateTimePicker datetimepicker, int i, int j, int k, int l, int i1);
    }


    public DateTimePicker(Context context) {
        this(context, System.currentTimeMillis());
    }

    public DateTimePicker(Context context, long l) {
        this(context, l, DateFormat.is24HourFormat(context));
    }

    public DateTimePicker(Context context, long l, boolean flag) {
        super(context);
        mIsEnabled = true;
        mDate = Calendar.getInstance();
        mInitialising = true;
        boolean flag1;
        String as[];
        if(getCurrentHourOfDay() >= 12)
            flag1 = true;
        else
            flag1 = false;
        mIsAm = flag1;
        inflate(context, 0x603001f, this);
        mDateSpinner = (NumberPicker)findViewById(0x60b0035);
        mDateSpinner.setMinValue(0);
        mDateSpinner.setMaxValue(6);
        mDateSpinner.setOnValueChangedListener(mOnDateChangedListener);
        mHourSpinner = (NumberPicker)findViewById(0x60b0045);
        mHourSpinner.setOnValueChangedListener(mOnHourChangedListener);
        mMinuteSpinner = (NumberPicker)findViewById(0x60b0046);
        mMinuteSpinner.setMinValue(0);
        mMinuteSpinner.setMaxValue(59);
        mMinuteSpinner.setOnLongPressUpdateInterval(100L);
        mMinuteSpinner.setOnValueChangedListener(mOnMinuteChangedListener);
        mDateDisplayValues = new String[7];
        as = (new DateFormatSymbols()).getAmPmStrings();
        mAmPmSpinner = (NumberPicker)findViewById(0x60b0047);
        mAmPmSpinner.setMinValue(0);
        mAmPmSpinner.setMaxValue(1);
        mAmPmSpinner.setDisplayedValues(as);
        mAmPmSpinner.setOnValueChangedListener(mOnAmPmChangedListener);
        updateDateControl();
        updateHourControl();
        updateAmPmControl();
        set24HourView(flag);
        setCurrentDate(l);
        setEnabled(isEnabled());
        mInitialising = false;
    }

    private int getCurrentHour() {
        if(!mIs24HourView) goto _L2; else goto _L1
_L1:
        int i = getCurrentHourOfDay();
_L4:
        return i;
_L2:
        i = getCurrentHourOfDay();
        if(i > 12)
            i -= 12;
        else
        if(i == 0)
            i = 12;
        if(true) goto _L4; else goto _L3
_L3:
    }

    private void onDateTimeChanged() {
        if(mOnDateTimeChangedListener != null)
            mOnDateTimeChangedListener.onDateTimeChanged(this, getCurrentYear(), getCurrentMonth(), getCurrentDay(), getCurrentHourOfDay(), getCurrentMinute());
    }

    private void updateAmPmControl() {
        if(mIs24HourView) {
            mAmPmSpinner.setVisibility(8);
        } else {
            int i;
            if(mIsAm)
                i = 0;
            else
                i = 1;
            mAmPmSpinner.setValue(i);
            mAmPmSpinner.setVisibility(0);
        }
    }

    private void updateDateControl() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mDate.getTimeInMillis());
        calendar.add(6, -4);
        mDateSpinner.setDisplayedValues(null);
        for(int i = 0; i < 7; i++) {
            calendar.add(6, 1);
            mDateDisplayValues[i] = (String)DateFormat.format("MM.dd EEEE", calendar);
        }

        mDateSpinner.setDisplayedValues(mDateDisplayValues);
        mDateSpinner.setValue(3);
        mDateSpinner.invalidate();
    }

    private void updateHourControl() {
        if(mIs24HourView) {
            mHourSpinner.setMinValue(0);
            mHourSpinner.setMaxValue(23);
        } else {
            mHourSpinner.setMinValue(1);
            mHourSpinner.setMaxValue(12);
        }
    }

    public long getCurrentDateInTimeMillis() {
        return mDate.getTimeInMillis();
    }

    public int getCurrentDay() {
        return mDate.get(5);
    }

    public int getCurrentHourOfDay() {
        return mDate.get(11);
    }

    public int getCurrentMinute() {
        return mDate.get(12);
    }

    public int getCurrentMonth() {
        return mDate.get(2);
    }

    public int getCurrentYear() {
        return mDate.get(1);
    }

    public boolean is24HourView() {
        return mIs24HourView;
    }

    public boolean isEnabled() {
        return mIsEnabled;
    }

    public void set24HourView(boolean flag) {
        if(mIs24HourView != flag) {
            mIs24HourView = flag;
            NumberPicker numberpicker = mAmPmSpinner;
            byte byte0;
            int i;
            if(flag)
                byte0 = 8;
            else
                byte0 = 0;
            numberpicker.setVisibility(byte0);
            i = getCurrentHourOfDay();
            updateHourControl();
            setCurrentHour(i);
            updateAmPmControl();
        }
    }

    public void setCurrentDate(int i, int j, int k, int l, int i1) {
        setCurrentYear(i);
        setCurrentMonth(j);
        setCurrentDay(k);
        setCurrentHour(l);
        setCurrentMinute(i1);
    }

    public void setCurrentDate(long l) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(l);
        setCurrentDate(calendar.get(1), calendar.get(2), calendar.get(5), calendar.get(11), calendar.get(12));
    }

    public void setCurrentDay(int i) {
        if(mInitialising || i != getCurrentDay()) {
            mDate.set(5, i);
            updateDateControl();
            onDateTimeChanged();
        }
    }

    public void setCurrentHour(int i) {
        if(mInitialising || i != getCurrentHourOfDay()) {
            mDate.set(11, i);
            if(!mIs24HourView) {
                if(i >= 12) {
                    mIsAm = false;
                    if(i > 12)
                        i -= 12;
                } else {
                    mIsAm = true;
                    if(i == 0)
                        i = 12;
                }
                updateAmPmControl();
            }
            mHourSpinner.setValue(i);
            onDateTimeChanged();
        }
    }

    public void setCurrentMinute(int i) {
        if(mInitialising || i != getCurrentMinute()) {
            mMinuteSpinner.setValue(i);
            mDate.set(12, i);
            onDateTimeChanged();
        }
    }

    public void setCurrentMonth(int i) {
        if(mInitialising || i != getCurrentMonth()) {
            mDate.set(2, i);
            updateDateControl();
            onDateTimeChanged();
        }
    }

    public void setCurrentYear(int i) {
        if(mInitialising || i != getCurrentYear()) {
            mDate.set(1, i);
            updateDateControl();
            onDateTimeChanged();
        }
    }

    public void setEnabled(boolean flag) {
        if(mIsEnabled != flag) {
            super.setEnabled(flag);
            mDateSpinner.setEnabled(flag);
            mMinuteSpinner.setEnabled(flag);
            mHourSpinner.setEnabled(flag);
            mAmPmSpinner.setEnabled(flag);
            mIsEnabled = flag;
        }
    }

    public void setOnDateTimeChangedListener(OnDateTimeChangedListener ondatetimechangedlistener) {
        mOnDateTimeChangedListener = ondatetimechangedlistener;
    }

    private static final int AMPM_SPINNER_MAX_VAL = 1;
    private static final int AMPM_SPINNER_MIN_VAL = 0;
    private static final int DATE_SPINNER_MAX_VAL = 6;
    private static final int DATE_SPINNER_MIN_VAL = 0;
    private static final int DAYS_IN_ALL_WEEK = 7;
    private static final boolean DEFAULT_ENABLE_STATE = true;
    private static final int HOURS_IN_ALL_DAY = 24;
    private static final int HOURS_IN_HALF_DAY = 12;
    private static final int HOUR_SPINNER_MAX_VAL_12_HOUR_VIEW = 12;
    private static final int HOUR_SPINNER_MAX_VAL_24_HOUR_VIEW = 23;
    private static final int HOUR_SPINNER_MIN_VAL_12_HOUR_VIEW = 1;
    private static final int HOUR_SPINNER_MIN_VAL_24_HOUR_VIEW = 0;
    private static final int MINUT_SPINNER_MAX_VAL = 59;
    private static final int MINUT_SPINNER_MIN_VAL;
    private final NumberPicker mAmPmSpinner;
    private Calendar mDate;
    private String mDateDisplayValues[];
    private final NumberPicker mDateSpinner;
    private final NumberPicker mHourSpinner;
    private boolean mInitialising;
    private boolean mIs24HourView;
    private boolean mIsAm;
    private boolean mIsEnabled;
    private final NumberPicker mMinuteSpinner;
    private android.widget.NumberPicker.OnValueChangeListener mOnAmPmChangedListener = new android.widget.NumberPicker.OnValueChangeListener() {

        public void onValueChange(NumberPicker numberpicker, int i, int j) {
            DateTimePicker datetimepicker = DateTimePicker.this;
            boolean flag2;
            if(!mIsAm)
                flag2 = true;
            else
                flag2 = false;
            datetimepicker.mIsAm = flag2;
            if(mIsAm)
                mDate.add(11, -12);
            else
                mDate.add(11, 12);
            updateAmPmControl();
            onDateTimeChanged();
        }

        final DateTimePicker this$0;

             {
                this$0 = DateTimePicker.this;
                super();
            }
    };
    private android.widget.NumberPicker.OnValueChangeListener mOnDateChangedListener = new android.widget.NumberPicker.OnValueChangeListener() {

        public void onValueChange(NumberPicker numberpicker, int i, int j) {
            mDate.add(6, j - i);
            updateDateControl();
            onDateTimeChanged();
        }

        final DateTimePicker this$0;

             {
                this$0 = DateTimePicker.this;
                super();
            }
    };
    private OnDateTimeChangedListener mOnDateTimeChangedListener;
    private android.widget.NumberPicker.OnValueChangeListener mOnHourChangedListener = new android.widget.NumberPicker.OnValueChangeListener() {

        public void onValueChange(NumberPicker numberpicker, int i, int j) {
            boolean flag2;
            Calendar calendar;
            flag2 = false;
            calendar = Calendar.getInstance();
            if(mIs24HourView) goto _L2; else goto _L1
_L1:
            int k;
            int j1;
            if(!mIsAm && i == 11 && j == 12) {
                calendar.setTimeInMillis(mDate.getTimeInMillis());
                calendar.add(6, 1);
                flag2 = true;
            } else
            if(mIsAm && i == 12 && j == 11) {
                calendar.setTimeInMillis(mDate.getTimeInMillis());
                calendar.add(6, -1);
                flag2 = true;
            }
            if(i == 11 && j == 12 || i == 12 && j == 11) {
                DateTimePicker datetimepicker = DateTimePicker.this;
                int i1;
                boolean flag3;
                if(!mIsAm)
                    flag3 = true;
                else
                    flag3 = false;
                datetimepicker.mIsAm = flag3;
                updateAmPmControl();
            }
            i1 = mHourSpinner.getValue() % 12;
            if(mIsAm)
                j1 = 0;
            else
                j1 = 12;
            k = i1 + j1;
_L4:
            mDate.set(11, k);
            onDateTimeChanged();
            if(flag2) {
                setCurrentYear(calendar.get(1));
                setCurrentMonth(calendar.get(2));
                setCurrentDay(calendar.get(5));
            }
            return;
_L2:
            if(i != 23 || j != 0)
                break; /* Loop/switch isn't completed */
            calendar.setTimeInMillis(mDate.getTimeInMillis());
            calendar.add(6, 1);
            flag2 = true;
_L6:
            k = mHourSpinner.getValue();
            if(true) goto _L4; else goto _L3
_L3:
            if(i != 0 || j != 23) goto _L6; else goto _L5
_L5:
            calendar.setTimeInMillis(mDate.getTimeInMillis());
            calendar.add(6, -1);
            flag2 = true;
              goto _L6
        }

        final DateTimePicker this$0;

             {
                this$0 = DateTimePicker.this;
                super();
            }
    };
    private android.widget.NumberPicker.OnValueChangeListener mOnMinuteChangedListener = new android.widget.NumberPicker.OnValueChangeListener() {

        public void onValueChange(NumberPicker numberpicker, int i, int j) {
            int k = mMinuteSpinner.getMinValue();
            int i1 = mMinuteSpinner.getMaxValue();
            int j1 = 0;
            if(i == i1 && j == k)
                j1 = 0 + 1;
            else
            if(i == k && j == i1)
                j1 = 0 - 1;
            if(j1 != 0) {
                mDate.add(11, j1);
                mHourSpinner.setValue(getCurrentHour());
                updateDateControl();
                if(getCurrentHourOfDay() >= 12) {
                    mIsAm = false;
                    updateAmPmControl();
                } else {
                    mIsAm = true;
                    updateAmPmControl();
                }
            }
            mDate.set(12, j);
            onDateTimeChanged();
        }

        final DateTimePicker this$0;

             {
                this$0 = DateTimePicker.this;
                super();
            }
    };







/*
    static boolean access$402(DateTimePicker datetimepicker, boolean flag) {
        datetimepicker.mIsAm = flag;
        return flag;
    }

*/




}
