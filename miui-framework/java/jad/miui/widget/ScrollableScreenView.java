// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ScrollView;

// Referenced classes of package miui.widget:
//            ScreenView

public class ScrollableScreenView extends ScreenView {
    private class ScrollAnimation extends Animation {

        protected void applyTransformation(float f, Transformation transformation) {
            int i = (int)(f * (float)(mToY - mFromY));
            mScrollView.scrollTo(mScrollView.getScrollX(), i + mFromY);
        }

        private int mFromY;
        private int mToY;
        final ScrollableScreenView this$0;

        public ScrollAnimation(int i, int j) {
            this$0 = ScrollableScreenView.this;
            super();
            mFromY = 0;
            mToY = 0;
            mFromY = i;
            mToY = j;
            setDuration(500L);
        }
    }

    public static interface OnScrollOutListener {

        public abstract boolean onScrollOut(View view, int i);

        public static final int DIRECTION_LEFT = 0;
        public static final int DIRECTION_RIGHT = 1;
    }


    public ScrollableScreenView(Context context) {
        super(context);
    }

    public ScrollableScreenView(Context context, AttributeSet attributeset) {
        super(context, attributeset);
    }

    public ScrollableScreenView(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
    }

    private void makeWholeViewVisiable() {
        if(mScrollView != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        int i = getTop();
        ViewParent viewparent;
        for(viewparent = getParent(); viewparent != null && viewparent != mScrollView;)
            if(viewparent instanceof View) {
                i += ((View)viewparent).getTop();
                viewparent = viewparent.getParent();
            } else {
                viewparent = null;
            }

        if(viewparent == mScrollView && mScrollView.getScrollY() > i)
            mScrollView.startAnimation(new ScrollAnimation(mScrollView.getScrollY(), i));
        if(true) goto _L1; else goto _L3
_L3:
    }

    public boolean onTouchEvent(MotionEvent motionevent) {
        if(2 == (0xff & motionevent.getAction()))
            requestDisallowInterceptTouchEvent(true);
        return super.onTouchEvent(motionevent);
    }

    public void setOnScrollOutListener(OnScrollOutListener onscrolloutlistener) {
        mScrollOutListener = onscrolloutlistener;
    }

    public void setParentScrollView(ScrollView scrollview) {
        mScrollView = scrollview;
        if(mScrollView != null)
            setScreenSnapDuration(100);
    }

    protected void snapToScreen(int i, int j, boolean flag) {
        super.mNextScreen = Math.max(0, Math.min(i, getScreenCount() - super.mVisibleRange));
        if(super.mNextScreen == super.mCurrentScreen && mScrollOutListener != null) {
            if((double)super.mScrollX < (double)super.mScrollLeftBound + 0.20000000000000001D * (double)((float)super.mChildScreenWidth * super.mOverScrollRatio) && mScrollOutListener.onScrollOut(this, 0))
                setCurrentScreen(-1 + getScreenCount());
            else
            if((double)super.mScrollX > (double)super.mScrollRightBound - 0.20000000000000001D * (double)((float)super.mChildScreenWidth * super.mOverScrollRatio) && mScrollOutListener.onScrollOut(this, 1))
                setCurrentScreen(0);
            else
                super.snapToScreen(i, j, flag);
        } else {
            super.snapToScreen(i, j, flag);
        }
        makeWholeViewVisiable();
    }

    private static final int DEFAULT_SCREEN_SNAP_DURATION = 100;
    private OnScrollOutListener mScrollOutListener;
    private ScrollView mScrollView;

}
