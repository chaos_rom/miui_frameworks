// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.analytics;

import android.database.Cursor;
import android.text.TextUtils;
import java.util.*;

// Referenced classes of package miui.analytics:
//            Event, Dispatchable, Storable

public class TrackEvent extends Event {

    public TrackEvent() {
        super.mType = Integer.valueOf(2);
        mParam = null;
        mValue = 0L;
    }

    public TrackEvent(String s, Map map, long l) {
        super.mType = Integer.valueOf(2);
        super.mEventId = s;
        mParam = map;
        mValue = l;
    }

    private String buildParam(Map map) {
        String s = "";
        if(map != null) {
            StringBuilder stringbuilder = new StringBuilder();
            for(Iterator iterator = map.keySet().iterator(); iterator.hasNext(); stringbuilder.append('$')) {
                String s1 = (String)iterator.next();
                stringbuilder.append(s1);
                stringbuilder.append('$');
                stringbuilder.append((String)map.get(s1));
            }

            s = stringbuilder.toString();
        }
        return s;
    }

    private void parseParam(String s) {
        if(!TextUtils.isEmpty(s)) {
            mParam = new HashMap();
            String as[] = s.split("\\$");
            for(int i = 0; i < -1 + as.length; i += 2)
                mParam.put(as[i], as[i + 1]);

        }
    }

    public void dispatch() {
        if(sDispatcher != null) {
            for(Iterator iterator = sDispatcher.iterator(); iterator.hasNext(); ((Dispatchable)iterator.next()).dispatchEvent(this));
        }
    }

    public Map getParam() {
        return mParam;
    }

    public long getValue() {
        return mValue;
    }

    public void restore(Cursor cursor) {
        super.restore(cursor);
        if(cursor != null) {
            mValue = Long.parseLong(cursor.getString(cursor.getColumnIndexOrThrow("value")));
            parseParam(cursor.getString(cursor.getColumnIndexOrThrow("param")));
        }
    }

    public void writeEvent(Storable storable) {
        if(storable != null)
            storable.writeData(super.mType, super.mEventId, buildParam(mParam), (new StringBuilder()).append(super.mTrackTime).append("").toString(), (new StringBuilder()).append(mValue).append("").toString());
    }

    private Map mParam;
    private long mValue;
}
