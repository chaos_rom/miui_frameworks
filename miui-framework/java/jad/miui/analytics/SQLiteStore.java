// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.analytics;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.*;
import android.util.Log;

// Referenced classes of package miui.analytics:
//            Storable, AnalyticsDatabaseHelper

class SQLiteStore
    implements Storable {

    SQLiteStore() {
    }

    public void close() {
        if(mDatabaseHelper != null)
            mDatabaseHelper.close();
    }

    public void create(Context context, String s) {
        mDatabaseHelper = new AnalyticsDatabaseHelper(context, s, "analytics", 2);
    }

    public Cursor readDataset(String s) {
        Cursor cursor;
        String s1;
        cursor = null;
        if(mDatabaseHelper == null)
            break MISSING_BLOCK_LABEL_67;
        s1 = "select * from analytics ";
        if(s != null) {
            Object aobj1[] = new Object[2];
            aobj1[0] = "select * from analytics ";
            aobj1[1] = s;
            s1 = String.format("%s where %s", aobj1);
        }
        Cursor cursor1;
        SQLiteDatabase sqlitedatabase = mDatabaseHelper.getReadableDatabase();
        if(sqlitedatabase == null)
            break MISSING_BLOCK_LABEL_67;
        cursor1 = sqlitedatabase.rawQuery(s1, null);
        cursor = cursor1;
_L2:
        return cursor;
        SQLiteException sqliteexception;
        sqliteexception;
        Object aobj[] = new Object[1];
        aobj[0] = mDatabaseHelper.getDatabaseName();
        Log.e("ANALYTICS.SQLITESTORE", String.format("can't read database:%s", aobj));
        if(true) goto _L2; else goto _L1
_L1:
    }

    public void writeData(Integer integer, String s, String s1, String s2, String s3) {
        if(mDatabaseHelper == null)
            break MISSING_BLOCK_LABEL_84;
        SQLiteDatabase sqlitedatabase = mDatabaseHelper.getWritableDatabase();
        if(sqlitedatabase != null) {
            Object aobj1[] = new Object[1];
            aobj1[0] = "analytics";
            String s4 = String.format("insert into %s values(null, ?, ?, ?, ?, ?)", aobj1);
            Object aobj2[] = new Object[5];
            aobj2[0] = integer;
            aobj2[1] = s;
            aobj2[2] = s1;
            aobj2[3] = s2;
            aobj2[4] = s3;
            sqlitedatabase.execSQL(s4, aobj2);
        }
_L1:
        return;
        SQLiteException sqliteexception;
        sqliteexception;
        Object aobj[] = new Object[1];
        aobj[0] = mDatabaseHelper.getDatabaseName();
        Log.e("ANALYTICS.SQLITESTORE", String.format("database:%s is not writable!", aobj));
          goto _L1
    }

    private static final String INSERT_ITEM = "insert into %s values(null, ?, ?, ?, ?, ?)";
    private static final String QUERY_ITEMS = "select * from analytics ";
    private static final String TABLE_NAME = "analytics";
    public static final String TAG = "ANALYTICS.SQLITESTORE";
    private static final int VERSION = 2;
    private SQLiteOpenHelper mDatabaseHelper;
}
