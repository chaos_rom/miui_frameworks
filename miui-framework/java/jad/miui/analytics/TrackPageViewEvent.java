// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.analytics;

import java.util.Iterator;
import java.util.List;

// Referenced classes of package miui.analytics:
//            Event, Dispatchable, Storable

public class TrackPageViewEvent extends Event {

    public TrackPageViewEvent() {
        super.mType = Integer.valueOf(3);
        super.mEventId = "_pageview_event_";
    }

    public void dispatch() {
        if(sDispatcher != null) {
            for(Iterator iterator = sDispatcher.iterator(); iterator.hasNext(); ((Dispatchable)iterator.next()).dispatchPageView(this));
        }
    }

    public void writeEvent(Storable storable) {
        if(storable != null)
            storable.writeData(super.mType, super.mEventId, "", (new StringBuilder()).append(super.mTrackTime).append("").toString(), Boolean.toString(false));
    }

    private static final String PAGEVIEW_EVENT = "_pageview_event_";
}
