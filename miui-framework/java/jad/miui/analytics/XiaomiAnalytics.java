// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.analytics;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.Log;
import java.util.*;

// Referenced classes of package miui.analytics:
//            PersistenceHelper, TrackEvent, TrackPageViewEvent, LogEvent

public class XiaomiAnalytics {

    private XiaomiAnalytics() {
        mPersistenceHelper = null;
        mUseHttps = false;
        mContext = null;
        mTimedEvents = null;
        mCount = 0;
    }

    private String getAppVersion() {
        String s = "";
        try {
            s = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).versionName;
        }
        catch(Exception exception) {
            exception.printStackTrace();
        }
        return s;
    }

    public static XiaomiAnalytics getInstance() {
        return sInstance;
    }

    private boolean isTrackedReady() {
        boolean flag;
        if(mPersistenceHelper == null) {
            Log.i("XIAOMIANALYTICS", "method: startSession should be called before tracking events");
            flag = false;
        } else {
            flag = true;
        }
        return flag;
    }

    /**
     * @deprecated Method endSession is deprecated
     */

    public void endSession() {
        this;
        JVM INSTR monitorenter ;
        if(mCount <= 0) goto _L2; else goto _L1
_L1:
        int i;
        i = -1 + mCount;
        mCount = i;
        if(i != 0) goto _L2; else goto _L3
_L3:
        boolean flag = isTrackedReady();
        if(flag) goto _L4; else goto _L2
_L2:
        this;
        JVM INSTR monitorexit ;
        return;
_L4:
        Object aobj[] = new Object[1];
        aobj[0] = mContext.getPackageName();
        Log.i("XIAOMIANALYTICS", String.format("end session(%s)", aobj));
        mContext = null;
        mPersistenceHelper.close();
        mPersistenceHelper = null;
        mTimedEvents.clear();
        mTimedEvents = null;
        if(true) goto _L2; else goto _L5
_L5:
        Exception exception;
        exception;
        throw exception;
    }

    public void endTimedEvent(String s) {
        if(mTimedEvents != null) goto _L2; else goto _L1
_L1:
        Log.i("XIAOMIANALYTICS", "there is no timed event");
_L4:
        return;
_L2:
        boolean flag = false;
        Iterator iterator = mTimedEvents.iterator();
        do {
            if(!iterator.hasNext())
                break;
            TrackEvent trackevent = (TrackEvent)iterator.next();
            if(!s.equals(trackevent.getEventId()))
                continue;
            flag = true;
            long l = System.currentTimeMillis();
            HashMap hashmap = new HashMap();
            hashmap.put("_timed_event_id_", s);
            trackEvent("_timed_event_", hashmap, l - trackevent.getTrackTime());
            iterator.remove();
            break;
        } while(true);
        if(!flag) {
            Object aobj[] = new Object[1];
            aobj[0] = s;
            Log.i("XIAOMIANALYTICS", String.format("the ended event (%s) is not timed", aobj));
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public void onTrackPageView() {
        if(isTrackedReady())
            mPersistenceHelper.writeEvent(new TrackPageViewEvent());
    }

    public void setUseHttps(boolean flag) {
        if(flag != mUseHttps)
            mUseHttps = flag;
    }

    /**
     * @deprecated Method startSession is deprecated
     */

    public void startSession(Context context) {
        this;
        JVM INSTR monitorenter ;
        if(context == null)
            break MISSING_BLOCK_LABEL_153;
        int i = mCount;
        mCount = i + 1;
        if(i == 0) {
            mContext = context.getApplicationContext();
            mPersistenceHelper = new PersistenceHelper();
            mPersistenceHelper.writeOpen(mContext);
            mTimedEvents = Collections.synchronizedList(new ArrayList());
            HashMap hashmap = new HashMap();
            hashmap.put("_android_version_", android.os.Build.VERSION.RELEASE);
            hashmap.put("_miui_version_", android.os.Build.VERSION.INCREMENTAL);
            hashmap.put("_app_version_", getAppVersion());
            trackEvent("_session_event_", hashmap);
            Object aobj[] = new Object[1];
            aobj[0] = mContext.getPackageName();
            Log.i("XIAOMIANALYTICS", String.format("start session(%s)", aobj));
        }
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    public void trackError(String s, String s1, String s2) {
        if(!TextUtils.isEmpty(s) && !TextUtils.isEmpty(s2)) goto _L2; else goto _L1
_L1:
        Log.i("XIAOMIANALYTICS", "the id or error class of loged event is null or empty");
_L4:
        return;
_L2:
        if(isTrackedReady()) {
            if(s1 == null)
                s1 = "";
            mPersistenceHelper.writeEvent(new LogEvent(s, s1, s2));
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public void trackEvent(String s) {
        trackTimedEvent(s, null, false, 0L);
    }

    public void trackEvent(String s, long l) {
        trackTimedEvent(s, null, false, l);
    }

    public void trackEvent(String s, Object obj) {
        HashMap hashmap = new HashMap();
        hashmap.put("_event_default_param_", obj.toString());
        trackEvent(s, ((Map) (hashmap)));
    }

    public void trackEvent(String s, Map map) {
        trackTimedEvent(s, map, false, 0L);
    }

    public void trackEvent(String s, Map map, long l) {
        trackTimedEvent(s, map, false, l);
    }

    public void trackTimedEvent(String s, Map map, boolean flag) {
        trackTimedEvent(s, map, flag, 0L);
    }

    public void trackTimedEvent(String s, Map map, boolean flag, long l) {
        if(!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        Log.i("XIAOMIANALYTICS", "the id of tracked event is null or empty");
_L4:
        return;
_L2:
        if(isTrackedReady()) {
            TrackEvent trackevent = new TrackEvent(s, map, l);
            mPersistenceHelper.writeEvent(trackevent);
            if(flag)
                mTimedEvents.add(trackevent);
        }
        if(true) goto _L4; else goto _L3
_L3:
    }

    public void trackTimedEvent(String s, boolean flag) {
        trackTimedEvent(s, null, flag, 0L);
    }

    public void trackTimedEvent(String s, boolean flag, long l) {
        trackTimedEvent(s, null, flag, 0L);
    }

    private static final String ANDROID_VERSION = "_android_version_";
    private static final String APP_VERSION = "_app_version_";
    private static final long DEFAULT_EVENT_VALUE = 0L;
    private static final String EVENT_DEFAULT_PARAM = "_event_default_param_";
    private static final String MIUI_VERSION = "_miui_version_";
    private static final String SESSION_EVENT = "_session_event_";
    private static final String TAG = "XIAOMIANALYTICS";
    private static final String TIMED_EVENT = "_timed_event_";
    private static final String TIMED_EVENT_ID = "_timed_event_id_";
    private static final XiaomiAnalytics sInstance = new XiaomiAnalytics();
    private Context mContext;
    private int mCount;
    private PersistenceHelper mPersistenceHelper;
    private List mTimedEvents;
    private boolean mUseHttps;

}
