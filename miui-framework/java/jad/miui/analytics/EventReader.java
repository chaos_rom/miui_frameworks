// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.analytics;

import android.content.Context;
import java.util.*;

// Referenced classes of package miui.analytics:
//            PersistenceHelper

public class EventReader {

    public EventReader() {
        mPersistenceHelper = new PersistenceHelper();
    }

    public void close() {
        if(mPersistenceHelper != null)
            mPersistenceHelper.close();
    }

    public void open(Context context, String s) {
        mPersistenceHelper.readOpen(context, s);
    }

    public List readEvents(String s, Map map) {
        List list;
        if(mPersistenceHelper != null)
            list = mPersistenceHelper.readEvents(s, map);
        else
            list = Collections.emptyList();
        return list;
    }

    private PersistenceHelper mPersistenceHelper;
}
