// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.analytics;

import android.database.Cursor;
import android.text.TextUtils;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package miui.analytics:
//            Event, Dispatchable, Storable

public class LogEvent extends Event {

    public LogEvent() {
        super.mType = Integer.valueOf(1);
        mMessage = "";
        mErrorClass = "";
    }

    public LogEvent(String s, String s1, String s2) {
        super.mType = Integer.valueOf(1);
        super.mEventId = s;
        mMessage = s1;
        mErrorClass = s2;
    }

    private String buildParam() {
        StringBuilder stringbuilder = new StringBuilder(mMessage);
        stringbuilder.append('$');
        stringbuilder.append(mErrorClass);
        return stringbuilder.toString();
    }

    private void parseParam(String s) {
        if(!TextUtils.isEmpty(s)) {
            String as[] = s.split("\\$");
            if(as.length > 1) {
                mMessage = as[0];
                mErrorClass = as[1];
            }
        }
    }

    public void dispatch() {
        if(sDispatcher != null) {
            for(Iterator iterator = sDispatcher.iterator(); iterator.hasNext(); ((Dispatchable)iterator.next()).dispatchLog(this));
        }
    }

    public String getErrorClass() {
        return mErrorClass;
    }

    public String getMessage() {
        return mMessage;
    }

    public void restore(Cursor cursor) {
        super.restore(cursor);
        if(cursor != null)
            parseParam(cursor.getString(cursor.getColumnIndexOrThrow("param")));
    }

    public void writeEvent(Storable storable) {
        if(storable != null)
            storable.writeData(super.mType, super.mEventId, buildParam(), (new StringBuilder()).append(super.mTrackTime).append("").toString(), Boolean.toString(false));
    }

    private String mErrorClass;
    private String mMessage;
}
