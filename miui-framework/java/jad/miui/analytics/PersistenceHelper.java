// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.analytics;

import android.content.Context;
import android.database.Cursor;
import android.os.*;
import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// Referenced classes of package miui.analytics:
//            ObjectBuilder, TrackEvent, LogEvent, TrackPageViewEvent, 
//            SQLiteStore, Storable, EventUtils, Event

class PersistenceHelper {
    private class RunThread extends Thread {

        public void run() {
            Looper.prepare();
            synchronized(mSynchronized) {
                mWorkHandler = new Handler() {

                    public void handleMessage(Message message) {
                        message.what;
                        JVM INSTR tableswitch 1 3: default 32
                    //                                   1 33
                    //                                   2 46
                    //                                   3 66;
                           goto _L1 _L2 _L3 _L4
_L1:
                        return;
_L2:
                        writeOpenImp();
                        continue; /* Loop/switch isn't completed */
_L3:
                        writeEventImp((Event)message.obj);
                        continue; /* Loop/switch isn't completed */
_L4:
                        closeImp();
                        mWorkHandler.getLooper().quit();
                        mWorkHandler = null;
                        if(true) goto _L1; else goto _L5
_L5:
                    }

                    final RunThread this$1;

                 {
                    this$1 = RunThread.this;
                    super();
                }
                };
                mSynchronized.notify();
            }
            Looper.loop();
            return;
            exception;
            obj;
            JVM INSTR monitorexit ;
            throw exception;
        }

        final PersistenceHelper this$0;

        private RunThread() {
            this$0 = PersistenceHelper.this;
            super();
        }

    }

    private static final class Mode extends Enum {

        public static Mode valueOf(String s) {
            return (Mode)Enum.valueOf(miui/analytics/PersistenceHelper$Mode, s);
        }

        public static Mode[] values() {
            return (Mode[])$VALUES.clone();
        }

        private static final Mode $VALUES[];
        public static final Mode READ_ONLY;
        public static final Mode READ_WRITE;

        static  {
            READ_WRITE = new Mode("READ_WRITE", 0);
            READ_ONLY = new Mode("READ_ONLY", 1);
            Mode amode[] = new Mode[2];
            amode[0] = READ_WRITE;
            amode[1] = READ_ONLY;
            $VALUES = amode;
        }

        private Mode(String s, int i) {
            super(s, i);
        }
    }


    public PersistenceHelper() {
        mEnableWrite = false;
        mMode = Mode.READ_WRITE;
        mStoreBuilder = new ObjectBuilder();
        mEventBuilder = new ObjectBuilder();
        mSynchronized = new Object();
        mEventBuilder.registerClass(miui/analytics/TrackEvent, "2");
        mEventBuilder.registerClass(miui/analytics/LogEvent, "1");
        mEventBuilder.registerClass(miui/analytics/TrackPageViewEvent, "3");
        mStoreBuilder.registerClass(miui/analytics/SQLiteStore, "ANALYTICS.SQLITESTORE");
    }

    private void closeImp() {
        if(mStore != null) {
            mStore.close();
            mStore = null;
        }
        mContext = null;
    }

    private void createStore(String s) {
        mStore = (Storable)mStoreBuilder.buildObject("ANALYTICS.SQLITESTORE");
        mStore.create(mContext, s);
    }

    private void deletePreDatabase(String s) {
        long l = EventUtils.getDay(System.currentTimeMillis());
        File file = mContext.getDatabasePath(s);
        if(file.exists() && l - (long)EventUtils.getDay(file.lastModified()) >= 7L)
            EventUtils.deleteDatabaseFile(mContext, s);
    }

    private void writeEventImp(Event event) {
        if(mStore != null && Mode.READ_WRITE == mMode && mEnableWrite)
            event.writeEvent(mStore);
    }

    private void writeOpenImp() {
        boolean flag = EventUtils.enableWrite(mContext);
        mEnableWrite = flag;
        if(flag) {
            String s = EventUtils.getDatabaseName();
            deletePreDatabase(s);
            createStore(s);
        }
    }

    public void close() {
        if(mWorkHandler != null) {
            Message message = new Message();
            message.what = 3;
            mWorkHandler.sendMessage(message);
        } else {
            closeImp();
        }
    }

    public List readEvents(String s, Map map) {
        if(mStore == null) goto _L2; else goto _L1
_L1:
        Cursor cursor = mStore.readDataset(s);
        if(cursor == null) goto _L2; else goto _L3
_L3:
        Object obj;
        obj = new ArrayList();
        try {
label0:
            do {
                if(!cursor.moveToNext())
                    break;
                int i = cursor.getInt(cursor.getColumnIndexOrThrow("type"));
                Event event = (Event)mEventBuilder.buildObject((new StringBuilder()).append(i).append("").toString());
                if(event == null)
                    continue;
                event.restore(cursor);
                Iterator iterator = map.keySet().iterator();
                Pattern pattern;
                do {
                    if(!iterator.hasNext())
                        continue label0;
                    pattern = (Pattern)iterator.next();
                } while(!pattern.matcher(event.getEventId()).matches());
                event.setPolicy((String)map.get(pattern));
                ((ArrayList) (obj)).add(event);
            } while(true);
        }
        catch(IllegalArgumentException illegalargumentexception) {
            illegalargumentexception.printStackTrace();
        }
        cursor.close();
_L5:
        return ((List) (obj));
_L2:
        obj = Collections.emptyList();
        if(true) goto _L5; else goto _L4
_L4:
    }

    public void readOpen(Context context, String s) {
        mContext = context.getApplicationContext();
        mMode = Mode.READ_ONLY;
        createStore(s);
    }

    public boolean selectStore(String s) {
        boolean flag = false;
        Storable storable = (Storable)mStoreBuilder.buildObject(s);
        if(storable != null) {
            if(mStore != null)
                mStore.close();
            mStore = storable;
            flag = true;
        }
        return flag;
    }

    public void writeEvent(String s, long l) {
        writeEvent(((Event) (new TrackEvent(s, null, l))));
    }

    public void writeEvent(Event event) {
        if(mWorkHandler != null) {
            Message message = new Message();
            message.what = 2;
            message.obj = event.clone();
            mWorkHandler.sendMessage(message);
        }
    }

    public void writeOpen(Context context) {
        mContext = context.getApplicationContext();
        mMode = Mode.READ_WRITE;
        Object obj = mSynchronized;
        obj;
        JVM INSTR monitorenter ;
        (new RunThread()).start();
        Exception exception;
        Message message;
        try {
            mSynchronized.wait();
        }
        catch(InterruptedException interruptedexception) { }
        message = new Message();
        message.what = 1;
        mWorkHandler.sendMessage(message);
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    private static final int MSG_CLOSE = 3;
    private static final int MSG_OPEN = 1;
    private static final int MSG_WRITE_EVENT = 2;
    private Context mContext;
    private boolean mEnableWrite;
    private ObjectBuilder mEventBuilder;
    private Mode mMode;
    private Storable mStore;
    private ObjectBuilder mStoreBuilder;
    private Object mSynchronized;
    private Handler mWorkHandler;




/*
    static Handler access$102(PersistenceHelper persistencehelper, Handler handler) {
        persistencehelper.mWorkHandler = handler;
        return handler;
    }

*/



}
