// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.analytics;

import java.util.ArrayList;
import java.util.Iterator;

public class ObjectBuilder {
    private class BuilderData {

        String mTag;
        Class mType;
        final ObjectBuilder this$0;

        BuilderData(Class class1, String s) {
            this$0 = ObjectBuilder.this;
            super();
            mType = class1;
            mTag = s;
        }
    }


    public ObjectBuilder() {
        mBuilderInf = new ArrayList();
    }

    public Object buildObject(String s) {
        Object obj;
        Iterator iterator;
        obj = null;
        iterator = mBuilderInf.iterator();
_L2:
        BuilderData builderdata;
        if(!iterator.hasNext())
            break; /* Loop/switch isn't completed */
        builderdata = (BuilderData)iterator.next();
        if(!builderdata.mTag.equals(s))
            continue; /* Loop/switch isn't completed */
        Object obj1 = builderdata.mType.newInstance();
        obj = obj1;
        continue; /* Loop/switch isn't completed */
        InstantiationException instantiationexception;
        instantiationexception;
        instantiationexception.printStackTrace();
        continue; /* Loop/switch isn't completed */
        IllegalAccessException illegalaccessexception;
        illegalaccessexception;
        illegalaccessexception.printStackTrace();
        if(true) goto _L2; else goto _L1
_L1:
        return obj;
    }

    public boolean registerClass(Class class1, String s) {
        Iterator iterator = mBuilderInf.iterator();
_L4:
        if(!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        if(!s.equals(((BuilderData)iterator.next()).mTag)) goto _L4; else goto _L3
_L3:
        boolean flag = false;
_L6:
        return flag;
_L2:
        flag = mBuilderInf.add(new BuilderData(class1, s));
        if(true) goto _L6; else goto _L5
_L5:
    }

    private ArrayList mBuilderInf;
}
