// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.analytics;

import android.content.Context;
import android.database.Cursor;

interface Storable {

    public abstract void close();

    public abstract void create(Context context, String s);

    public abstract Cursor readDataset(String s);

    public abstract void writeData(Integer integer, String s, String s1, String s2, String s3);
}
