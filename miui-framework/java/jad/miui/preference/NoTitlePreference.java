// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.preference;

import android.content.Context;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class NoTitlePreference extends Preference {

    public NoTitlePreference(Context context, AttributeSet attributeset) {
        super(context, attributeset);
    }

    protected View onCreateView(ViewGroup viewgroup) {
        View view = super.onCreateView(viewgroup);
        TextView textview = (TextView)view.findViewById(0x1020016);
        if(textview != null)
            textview.setVisibility(8);
        return view;
    }
}
