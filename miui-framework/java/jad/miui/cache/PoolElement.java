// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.cache;

import android.util.Poolable;

public class PoolElement
    implements Poolable {

    public PoolElement() {
        mPooled = false;
        mNext = null;
    }

    public Object getNextPoolable() {
        return mNext;
    }

    public boolean isPooled() {
        return mPooled;
    }

    public void setNextPoolable(Object obj) {
        mNext = obj;
    }

    public void setPooled(boolean flag) {
        mPooled = flag;
    }

    private Object mNext;
    private boolean mPooled;
}
