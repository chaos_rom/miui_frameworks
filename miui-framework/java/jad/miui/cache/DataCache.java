// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.cache;

import java.util.LinkedHashMap;
import java.util.Map;

public class DataCache extends LinkedHashMap {

    public DataCache(int i) {
        this(i, 0);
    }

    public DataCache(int i, int j) {
        this(i, j, 0.75F);
    }

    public DataCache(int i, int j, float f) {
        super(j, f, true);
        maximumCapacity = i;
    }

    public DataCache(Map map, int i) {
        this(i);
        putAll(map);
    }

    public int getMaximumCapacity() {
        return maximumCapacity;
    }

    protected boolean removeEldestEntry(java.util.Map.Entry entry) {
        boolean flag;
        if(size() > maximumCapacity)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public void setMaximumCapacity(int i) {
        maximumCapacity = i;
    }

    private static final long serialVersionUID = 1L;
    private int maximumCapacity;
}
