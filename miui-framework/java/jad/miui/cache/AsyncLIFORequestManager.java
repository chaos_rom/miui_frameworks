// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.cache;

import android.os.Handler;
import android.os.Message;
import android.util.*;
import java.util.*;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

// Referenced classes of package miui.cache:
//            RequestManager, PoolElement

public class AsyncLIFORequestManager extends RequestManager {
    private static class Worker {
        class LoopComputer
            implements Runnable {

            public void run() {
                Thread.currentThread().toString();
_L6:
                if(Thread.interrupted()) goto _L2; else goto _L1
_L1:
                if(mActive) goto _L4; else goto _L3
_L3:
                Object obj = mLock;
                obj;
                JVM INSTR monitorenter ;
                while(!mActive) 
                    mLock.wait();
                  goto _L5
                Exception exception1;
                exception1;
                RequestManager.Request request;
                try {
                    throw exception1;
                }
                catch(InterruptedException interruptedexception) { }
                finally {
                    throw exception;
                }
_L2:
                return;
_L5:
                obj;
                JVM INSTR monitorexit ;
_L4:
label0:
                {
                    request = (RequestManager.Request)mDeque.take();
                    if(request != null) {
                        if(!mActive)
                            break label0;
                        AsyncLIFORequestManager.onComputeAsync(mHandler, request, mPool);
                    }
                }
                  goto _L6
                mDeque.addFirst(request);
                  goto _L6
            }

            final Worker this$0;

            LoopComputer() {
                this$0 = Worker.this;
                super();
            }
        }


        public void dumpRequest(String s) {
            StringBuilder stringbuilder = new StringBuilder();
            stringbuilder.append(s);
            stringbuilder.append("  ");
            for(Iterator iterator = mDeque.iterator(); iterator.hasNext(); stringbuilder.append(", "))
                stringbuilder.append(Integer.toHexString(System.identityHashCode((RequestManager.Request)iterator.next())));

            Log.d(AsyncLIFORequestManager.TAG, stringbuilder.toString());
        }

        public void execute(RequestManager.Request request) {
            mDeque.addFirst(request);
            start();
        }

        public void quit() {
            for(int i = 0; i < mThreadIndex; i++) {
                mThreadPool[i].interrupt();
                mThreadPool[i] = null;
            }

            mThreadIndex = 0;
        }

        public boolean remove(RequestManager.Request request) {
            return mDeque.remove(request);
        }

        public void schedule(RequestManager.Request request) {
            if(mDeque.remove(request))
                execute(request);
        }

        public void setActive(boolean flag) {
            mActive = flag;
            if(flag) {
                synchronized(mLock) {
                    mLock.notifyAll();
                }
                start();
            }
            return;
            exception;
            obj;
            JVM INSTR monitorexit ;
            throw exception;
        }

        void start() {
            if(mThreadIndex < mThreadPool.length && !mDeque.isEmpty()) {
                int i = mThreadIndex;
                mThreadIndex = i + 1;
                Thread thread = new Thread(new LoopComputer(), String.valueOf(mThreadIndex));
                mThreadPool[i] = thread;
                thread.start();
            }
        }

        boolean mActive;
        final BlockingDeque mDeque = new LinkedBlockingDeque();
        final Handler mHandler;
        final Object mLock = new Object();
        final Pool mPool;
        int mThreadIndex;
        final Thread mThreadPool[];

        Worker(int i, Handler handler, Pool pool) {
            mThreadIndex = 0;
            mActive = false;
            mHandler = handler;
            mPool = pool;
            mThreadPool = new Thread[i];
        }
    }

    static class QueueHolder extends PoolElement {

        public final LinkedList mQueue = new LinkedList();

        QueueHolder() {
        }
    }

    static class Response extends PoolElement {

        public Object mKey;
        public Object mValue;

        Response() {
        }
    }


    public AsyncLIFORequestManager(int i, Object obj) {
        this(i, obj, 1, true);
    }

    public AsyncLIFORequestManager(int i, Object obj, int j, boolean flag) {
        super(i, obj);
        mResponsePool = Pools.synchronizedPool(Pools.finitePool(new PoolableManager() {

            public volatile Poolable newInstance() {
                return newInstance();
            }

            public Response newInstance() {
                return new Response();
            }

            public volatile void onAcquired(Poolable poolable) {
                onAcquired((Response)poolable);
            }

            public void onAcquired(Response response) {
            }

            public volatile void onReleased(Poolable poolable) {
                onReleased((Response)poolable);
            }

            public void onReleased(Response response) {
                response.mKey = null;
                response.mValue = null;
            }

            final AsyncLIFORequestManager this$0;

             {
                this$0 = AsyncLIFORequestManager.this;
                super();
            }
        }, 20));
        mQueuePool = Pools.finitePool(new PoolableManager() {

            public volatile Poolable newInstance() {
                return newInstance();
            }

            public QueueHolder newInstance() {
                return new QueueHolder();
            }

            public volatile void onAcquired(Poolable poolable) {
                onAcquired((QueueHolder)poolable);
            }

            public void onAcquired(QueueHolder queueholder) {
            }

            public volatile void onReleased(Poolable poolable) {
                onReleased((QueueHolder)poolable);
            }

            public void onReleased(QueueHolder queueholder) {
                queueholder.mQueue.clear();
            }

            final AsyncLIFORequestManager this$0;

             {
                this$0 = AsyncLIFORequestManager.this;
                super();
            }
        }, 10);
        mPendingMap = new HashMap();
        mRemoveKeyToKey = new HashMap();
        mWorker = null;
        mHandler = new Handler() {

            public void handleMessage(Message message) {
                if(message.what == 1) {
                    Response response = (Response)message.obj;
                    onComputeCompleted(response);
                    mResponsePool.release(response);
                }
            }

            final AsyncLIFORequestManager this$0;

             {
                this$0 = AsyncLIFORequestManager.this;
                super();
            }
        };
        mWorkThreadCount = j;
        mAutoTrim = flag;
    }

    static void onComputeAsync(Handler handler, RequestManager.Request request, Pool pool) {
        Object obj = request.computAsync();
        Message message = handler.obtainMessage(1);
        Response response = (Response)pool.acquire();
        response.mKey = request.getKey();
        response.mValue = obj;
        message.obj = response;
        handler.sendMessage(message);
    }

    private void trimPendings(RequestManager.Request request) {
        Object obj = mRemoveKeyToKey.put(request.getRemoveKey(), request.getKey());
        if(obj != null) {
            QueueHolder queueholder = (QueueHolder)mPendingMap.get(obj);
            if(queueholder != null) {
                LinkedList linkedlist = queueholder.mQueue;
                Iterator iterator = queueholder.mQueue.iterator();
                RequestManager.Request request1 = (RequestManager.Request)iterator.next();
                do {
                    if(!iterator.hasNext())
                        break;
                    RequestManager.Request request2 = (RequestManager.Request)iterator.next();
                    if(request2.isRemovable()) {
                        request2.onRemoved();
                        iterator.remove();
                    }
                } while(true);
                if(linkedlist.size() == 1 && request1.isRemovable() && mWorker.remove(request1)) {
                    request1.onRemoved();
                    mPendingMap.remove(obj);
                    mQueuePool.release(queueholder);
                }
            }
        }
    }

    void dumpPendings(String s) {
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder.append(s);
        stringbuilder.append("pending size=");
        stringbuilder.append(mPendingMap.size());
        stringbuilder.append("  ");
        Iterator iterator = mPendingMap.entrySet().iterator();
        int j;
        for(int i = 0; iterator.hasNext(); i = j) {
            StringBuilder stringbuilder1 = (new StringBuilder()).append("  pending ");
            j = i + 1;
            stringbuilder.append(stringbuilder1.append(i).toString());
            for(Iterator iterator1 = ((QueueHolder)((java.util.Map.Entry)iterator.next()).getValue()).mQueue.iterator(); iterator1.hasNext(); stringbuilder.append(", "))
                stringbuilder.append(Integer.toHexString(System.identityHashCode(iterator1.next())));

        }

        Log.d(TAG, stringbuilder.toString());
    }

    void onComputeCompleted(Response response) {
        Object obj = response.mKey;
        HashMap hashmap = mPendingMap;
        Object obj1 = response.mValue;
        if(obj1 == null)
            obj1 = super.mDefaultValue;
        boolean flag = false;
        QueueHolder queueholder = (QueueHolder)hashmap.remove(obj);
        if(queueholder != null) {
            LinkedList linkedlist = queueholder.mQueue;
            flag = ((RequestManager.Request)linkedlist.getLast()).needCache();
            for(Iterator iterator = linkedlist.iterator(); iterator.hasNext(); ((RequestManager.Request)iterator.next()).onCompleted(obj1, true));
            mQueuePool.release(queueholder);
        }
        if(flag)
            super.mCache.put(obj, obj1);
    }

    protected void onPause() {
        mWorker.setActive(false);
    }

    protected void onQuit() {
        if(mWorker != null) {
            mWorker.quit();
            mWorker = null;
            Object obj;
            for(Iterator iterator = mPendingMap.keySet().iterator(); iterator.hasNext(); super.mCache.remove(obj))
                obj = iterator.next();

            mPendingMap.clear();
        }
    }

    protected boolean onRequest(RequestManager.Request request) {
        boolean flag = false;
        Object obj = request.getKey();
        Object obj1 = super.mCache.get(obj);
        if(mAutoTrim)
            trimPendings(request);
        if(obj1 != null) {
            flag = true;
            request.onCompleted(obj1, true);
        } else {
            request.onCompleted(super.mDefaultValue, false);
            QueueHolder queueholder = (QueueHolder)mPendingMap.get(obj);
            if(queueholder == null) {
                QueueHolder queueholder1 = (QueueHolder)mQueuePool.acquire();
                queueholder1.mQueue.add(request);
                mPendingMap.put(obj, queueholder1);
                mWorker.execute(request);
            } else {
                LinkedList linkedlist = queueholder.mQueue;
                linkedlist.add(request);
                mWorker.schedule((RequestManager.Request)linkedlist.getFirst());
            }
        }
        return flag;
    }

    protected void onResume() {
        mWorker.setActive(true);
    }

    protected void onSetup() {
        mWorker = new Worker(mWorkThreadCount, mHandler, mResponsePool);
    }

    static final boolean DBG = false;
    static final int HOLDER_POOL_LIMIT = 20;
    private static final int MSG_RESPONSE = 1;
    static final int QUEUE_POOL_LIMIT = 10;
    static final String TAG = miui/cache/AsyncLIFORequestManager.getName();
    private final boolean mAutoTrim;
    private final Handler mHandler;
    private final HashMap mPendingMap;
    private Pool mQueuePool;
    private final HashMap mRemoveKeyToKey;
    Pool mResponsePool;
    private final int mWorkThreadCount;
    private Worker mWorker;

}
