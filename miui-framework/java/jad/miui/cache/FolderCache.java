// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.cache;

import java.io.File;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;
import miui.os.ExtraFileUtils;

// Referenced classes of package miui.cache:
//            DataCache

public class FolderCache {
    public static class FileInfo {

        public long length;
        public long modifiedTime;
        public String name;
        public String path;

        public FileInfo() {
        }
    }

    public static class FolderInfo {

        public Map files;
        public int filesCount;
        public long modifiedTime;
        public String name;
        public String path;

        public FolderInfo() {
        }
    }


    public FolderCache() {
        folderCache = new DataCache(10);
    }

    protected FileInfo buildFileInfo(String s, FolderInfo folderinfo) {
        File file = new File(s);
        FileInfo fileinfo = null;
        if(!file.isDirectory()) {
            fileinfo = newFileInfo();
            fileinfo.name = file.getName();
            fileinfo.path = s;
            fileinfo.modifiedTime = file.lastModified();
            fileinfo.length = file.length();
        }
        return fileinfo;
    }

    protected FolderInfo buildFolderInfo(String s) {
        File file = new File(s);
        FolderInfo folderinfo = null;
        if(file.isDirectory()) {
            folderinfo = newFolderInfo();
            folderinfo.name = file.getName();
            folderinfo.path = s;
            folderinfo.modifiedTime = file.lastModified();
            String as[] = file.list();
            int i;
            if(as == null)
                i = 0;
            else
                i = as.length;
            folderinfo.filesCount = i;
            folderinfo.files = new HashMap(folderinfo.filesCount);
            if(as != null) {
                for(int j = 0; j < as.length; j++) {
                    String s1 = (new StringBuilder()).append(s).append(as[j]).toString();
                    if(buildFileInfo(s1, folderinfo) != null)
                        folderinfo.files.put(s1, buildFileInfo(s1, folderinfo));
                }

            }
        }
        return folderinfo;
    }

    public FolderInfo get(String s) {
        String s1 = ExtraFileUtils.standardizeFolderPath(s);
        SoftReference softreference = (SoftReference)folderCache.get(s1);
        FolderInfo folderinfo;
        if(softreference == null)
            folderinfo = null;
        else
            folderinfo = (FolderInfo)softreference.get();
        if(!needRefresh(folderinfo)) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorenter ;
        if(needRefresh(folderinfo)) {
            folderinfo = buildFolderInfo(s1);
            if(folderinfo != null)
                folderCache.put(s1, new SoftReference(folderinfo));
        }
        this;
        JVM INSTR monitorexit ;
_L2:
        return folderinfo;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public boolean isCacheDirty(String s) {
        String s1 = ExtraFileUtils.standardizeFolderPath(s);
        SoftReference softreference = (SoftReference)folderCache.get(s1);
        FolderInfo folderinfo;
        if(softreference == null)
            folderinfo = null;
        else
            folderinfo = (FolderInfo)softreference.get();
        return needRefresh(folderinfo);
    }

    protected boolean needRefresh(FolderInfo folderinfo) {
        boolean flag = false;
        if(folderinfo == null) goto _L2; else goto _L1
_L1:
        File file = new File(folderinfo.path);
        long l = file.lastModified();
        String as[] = file.list();
        int i;
        if(as == null)
            i = 0;
        else
            i = as.length;
        if(folderinfo.modifiedTime != l || folderinfo.filesCount != i) goto _L2; else goto _L3
_L3:
        return flag;
_L2:
        flag = true;
        if(true) goto _L3; else goto _L4
_L4:
    }

    protected FileInfo newFileInfo() {
        return new FileInfo();
    }

    protected FolderInfo newFolderInfo() {
        return new FolderInfo();
    }

    private DataCache folderCache;
}
