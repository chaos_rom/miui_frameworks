// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package miui.cache;

import java.lang.ref.SoftReference;
import java.util.*;

// Referenced classes of package miui.cache:
//            AsyncLIFORequestManager

public abstract class RequestManager {
    static class BackupCache {

        public Object get(Object obj) {
            return mCacheHolder.get(obj);
        }

        public Object put(Object obj, Object obj1) {
            Object obj2;
            if(mCacheHolder != null) {
                obj2 = mCacheHolder.put(obj, obj1);
            } else {
                DataCache datacache = (DataCache)mCacheBackup.get();
                if(datacache != null)
                    obj2 = datacache.put(obj, obj1);
                else
                    obj2 = null;
            }
            return obj2;
        }

        public void quit() {
            mCacheBackup = new SoftReference(mCacheHolder);
            mCacheHolder = null;
        }

        public Object remove(Object obj) {
            Object obj1;
            if(mCacheHolder != null) {
                obj1 = mCacheHolder.remove(obj);
            } else {
                DataCache datacache = (DataCache)mCacheBackup.get();
                if(datacache != null)
                    obj1 = datacache.remove(obj);
                else
                    obj1 = null;
            }
            return obj1;
        }

        public void removeAll() {
            if(mCacheHolder == null) goto _L2; else goto _L1
_L1:
            mCacheHolder.clear();
_L4:
            return;
_L2:
            DataCache datacache = (DataCache)mCacheBackup.get();
            if(datacache != null)
                datacache.clear();
            if(true) goto _L4; else goto _L3
_L3:
        }

        public void setup() {
            if(mCacheBackup != null) {
                mCacheHolder = (DataCache)mCacheBackup.get();
                mCacheBackup = null;
            }
            if(mCacheHolder == null)
                mCacheHolder = new DataCache(mMaxSize, mExclude);
        }

        public Collection values() {
            Collection collection;
            if(mCacheHolder != null)
                collection = mCacheHolder.values();
            else
                collection = null;
            return collection;
        }

        private SoftReference mCacheBackup;
        private DataCache mCacheHolder;
        private final Object mExclude;
        private final int mMaxSize;

        public BackupCache(int i, Object obj) {
            mMaxSize = i;
            mExclude = obj;
        }
    }

    static class DataCache {

        public void clear() {
            mMap.clear();
            mExcludeKeys.clear();
        }

        public Object get(Object obj) {
            Object obj1 = mMap.get(obj);
            if(obj1 == null) {
                Object obj2;
                if(mExcludeKeys.contains(obj))
                    obj2 = mExclude;
                else
                    obj2 = null;
                obj1 = obj2;
            }
            return obj1;
        }

        public Object put(Object obj, Object obj1) {
            if(obj1 == mExclude) goto _L2; else goto _L1
_L1:
            Object obj2;
            obj2 = mMap.put(obj, obj1);
            if(obj2 == null)
                if(mExcludeKeys.remove(obj))
                    obj2 = mExclude;
                else
                    obj2 = null;
_L4:
            return obj2;
_L2:
            if(mExcludeKeys.add(obj))
                obj2 = null;
            else
                obj2 = mExclude;
            if(obj2 == null)
                obj2 = mMap.remove(obj);
            if(true) goto _L4; else goto _L3
_L3:
        }

        public Object remove(Object obj) {
            Object obj1 = mMap.remove(obj);
            if(obj1 == null) {
                Object obj2;
                if(mExcludeKeys.remove(obj))
                    obj2 = mExclude;
                else
                    obj2 = null;
                obj1 = obj2;
            }
            return obj1;
        }

        public Collection values() {
            Collection collection = mMap.values();
            if(!mExcludeKeys.isEmpty())
                collection.add(mExclude);
            return collection;
        }

        private final Object mExclude;
        private final Set mExcludeKeys = new HashSet();
        private final LinkedHashMap mMap;

        public DataCache(final int size, Object obj) {
            mMap = new LinkedHashMap() {

                protected boolean removeEldestEntry(java.util.Map.Entry entry) {
                    boolean flag;
                    if(size() > size)
                        flag = true;
                    else
                        flag = false;
                    return flag;
                }

                private static final long serialVersionUID = 1L;
                final DataCache this$0;
                final int val$size;

                 {
                    this$0 = DataCache.this;
                    size = i;
                    super();
                }
            };
            mExclude = obj;
        }
    }

    public static interface Request {

        public abstract Object computAsync();

        public abstract Object getKey();

        public abstract Object getRemoveKey();

        public abstract boolean isRemovable();

        public abstract boolean needCache();

        public abstract void onCompleted(Object obj, boolean flag);

        public abstract void onRemoved();
    }

    public static interface Detacher {

        public abstract void onDetach(Object obj);
    }


    public RequestManager(int i, Object obj) {
        mSetuped = false;
        mResumed = false;
        mCache = new BackupCache(i, obj);
        mDefaultValue = obj;
    }

    public static RequestManager create(int i, Object obj) {
        return new AsyncLIFORequestManager(i, obj);
    }

    public boolean isResumed() {
        return mResumed;
    }

    public boolean isStarted() {
        return mSetuped;
    }

    protected abstract void onPause();

    protected abstract void onQuit();

    protected abstract boolean onRequest(Request request1);

    protected abstract void onResume();

    protected abstract void onSetup();

    public void pause() {
        if(mResumed) {
            onPause();
            mResumed = false;
        }
    }

    public void quit(Detacher detacher) {
        if(mResumed)
            pause();
        if(mSetuped) {
            onQuit();
            Object obj = mDefaultValue;
            if(detacher != null) {
                Collection collection = mCache.values();
                if(collection != null) {
                    Iterator iterator = collection.iterator();
                    do {
                        if(!iterator.hasNext())
                            break;
                        Object obj1 = iterator.next();
                        if(obj != null)
                            detacher.onDetach(obj1);
                    } while(true);
                }
                if(obj != null)
                    detacher.onDetach(obj);
            }
            mCache.quit();
            mSetuped = false;
        }
    }

    public Object remove(Object obj) {
        return mCache.remove(obj);
    }

    public void removeAll() {
        mCache.removeAll();
    }

    public boolean request(Request request1) {
        boolean flag = false;
        if(request1.getKey() == null)
            request1.onCompleted(mDefaultValue, true);
        else
            flag = onRequest(request1);
        return flag;
    }

    public void resume() {
        if(mSetuped && !mResumed) {
            onResume();
            mResumed = true;
        }
    }

    public void setup(boolean flag) {
        if(!mSetuped) {
            mCache.setup();
            onSetup();
            mSetuped = true;
            if(flag)
                resume();
        }
    }

    protected final BackupCache mCache;
    protected final Object mDefaultValue;
    protected boolean mResumed;
    protected boolean mSetuped;
}
