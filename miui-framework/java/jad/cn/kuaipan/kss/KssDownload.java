// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package cn.kuaipan.kss;

import java.io.IOException;
import java.io.OutputStream;

public interface KssDownload {
    public static interface DownloadTransControl {
        public static final class EndState extends Enum {

            public static EndState valueOf(String s) {
                return (EndState)Enum.valueOf(cn/kuaipan/kss/KssDownload$DownloadTransControl$EndState, s);
            }

            public static EndState[] values() {
                return (EndState[])$VALUES.clone();
            }

            private static final EndState $VALUES[];
            public static final EndState Completed;
            public static final EndState DataCorrupted;
            public static final EndState Interrupt;
            public static final EndState Transing;

            static  {
                Completed = new EndState("Completed", 0);
                Interrupt = new EndState("Interrupt", 1);
                Transing = new EndState("Transing", 2);
                DataCorrupted = new EndState("DataCorrupted", 3);
                EndState aendstate[] = new EndState[4];
                aendstate[0] = Completed;
                aendstate[1] = Interrupt;
                aendstate[2] = Transing;
                aendstate[3] = DataCorrupted;
                $VALUES = aendstate;
            }

            private EndState(String s, int i) {
                super(s, i);
            }
        }


        public abstract int addData(byte abyte0[], int i) throws IOException;

        public abstract int getStartPos();

        public abstract void setEndState(EndState endstate);
    }

    public static interface BlockDownloadInfo {

        public abstract String getBlockDownload_FullURL(int i);

        public abstract int getBlockIndex();

        public abstract byte[] getBlockSHA1();

        public abstract int getBlockSize();

        public abstract int getDownloadURLCount();
    }

    public static interface RequestDownloadInfo {

        public abstract int getBlockCount();

        public abstract BlockDownloadInfo getBlockInfos(int i);

        public abstract int getFileSize();

        public abstract KssDef.KssAPIResult getResult();

        public abstract String getSecureKey();
    }


    public abstract KssDef.KssAPIResult download(DownloadTransControl downloadtranscontrol) throws Exception;

    public abstract KssDef.KssAPIResult download(OutputStream outputstream, int i) throws Exception;

    public abstract boolean init(RequestDownloadInfo requestdownloadinfo, KssDef.OnUpDownload onupdownload, KssDef.NetState netstate);

    public abstract void terminal();
}
