// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package cn.kuaipan.kss.implement;

import java.io.*;

public class KPDownloadTransControl
    implements cn.kuaipan.kss.KssDownload.DownloadTransControl {

    public KPDownloadTransControl() {
        m_curPos = 0;
        m_startPos = 0;
        m_endState = cn.kuaipan.kss.KssDownload.DownloadTransControl.EndState.Transing;
        m_outputStream = null;
    }

    private void _init() {
        m_curPos = 0;
        m_startPos = 0;
        m_endState = cn.kuaipan.kss.KssDownload.DownloadTransControl.EndState.Transing;
        m_outputStream = null;
    }

    public int addData(byte abyte0[], int i) throws IOException {
        if(m_outputStream == null) {
            throw new FileNotFoundException();
        } else {
            m_outputStream.write(abyte0, 0, i);
            m_curPos = i + m_curPos;
            return i;
        }
    }

    public void close() {
        if(m_outputStream == null)
            break MISSING_BLOCK_LABEL_21;
        m_outputStream.flush();
        m_outputStream.close();
_L2:
        return;
        Exception exception;
        exception;
        if(true) goto _L2; else goto _L1
_L1:
    }

    public int getCurrentPos() {
        return m_curPos;
    }

    public cn.kuaipan.kss.KssDownload.DownloadTransControl.EndState getEndState() {
        return m_endState;
    }

    public int getStartPos() {
        return m_startPos;
    }

    public void init(File file, boolean flag) throws FileNotFoundException {
        _init();
        if(!flag && file.exists())
            file.delete();
        m_outputStream = new FileOutputStream(file, flag);
        if(flag)
            setStartPos((int)file.length());
    }

    public void init(OutputStream outputstream) {
        _init();
        m_outputStream = outputstream;
    }

    public void setEndState(cn.kuaipan.kss.KssDownload.DownloadTransControl.EndState endstate) {
        m_endState = endstate;
    }

    public int setStartPos(int i) {
        if(i >= 0) {
            int j = m_curPos;
            m_startPos = i;
            m_curPos = i;
            i = j;
        }
        return i;
    }

    public void terminel() {
        close();
        m_outputStream = null;
    }

    private int m_curPos;
    private cn.kuaipan.kss.KssDownload.DownloadTransControl.EndState m_endState;
    private OutputStream m_outputStream;
    private int m_startPos;
}
