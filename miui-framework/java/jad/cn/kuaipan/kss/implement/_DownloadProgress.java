// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package cn.kuaipan.kss.implement;


class _DownloadProgress {

    _DownloadProgress() {
        m_onDownload = null;
        m_isCanceled = false;
        m_transSize = 0;
        m_totalSize = 0;
        m_lastSize = 0;
        m_notifySize = 0;
    }

    public void TransBytes(int i) {
        boolean flag = true;
        if(m_onDownload != null && i >= flag) goto _L2; else goto _L1
_L1:
        return;
_L2:
        m_transSize = i + m_transSize;
        if(m_transSize >= m_totalSize || m_transSize - m_lastSize >= m_notifySize) {
            if(m_onDownload.OnTransData(m_transSize))
                flag = false;
            m_isCanceled = flag;
            m_lastSize = m_transSize;
        }
        if(true) goto _L1; else goto _L3
_L3:
    }

    public boolean getIsCanceled() {
        return m_isCanceled;
    }

    public void init(cn.kuaipan.kss.KssDownload.RequestDownloadInfo requestdownloadinfo, cn.kuaipan.kss.KssDef.OnUpDownload onupdownload) {
        if(requestdownloadinfo != null) {
            m_onDownload = onupdownload;
            m_isCanceled = false;
            m_transSize = 0;
            m_totalSize = requestdownloadinfo.getFileSize();
            m_lastSize = 0;
            m_notifySize = m_totalSize / 50;
        }
    }

    public void setIsCanceled(boolean flag) {
        m_isCanceled = flag;
    }

    public void startTrans(cn.kuaipan.kss.KssDownload.DownloadTransControl downloadtranscontrol) {
        m_isCanceled = false;
        m_transSize = downloadtranscontrol.getStartPos();
        m_lastSize = m_transSize;
    }

    private boolean m_isCanceled;
    private int m_lastSize;
    private int m_notifySize;
    private cn.kuaipan.kss.KssDef.OnUpDownload m_onDownload;
    private int m_totalSize;
    private int m_transSize;
}
