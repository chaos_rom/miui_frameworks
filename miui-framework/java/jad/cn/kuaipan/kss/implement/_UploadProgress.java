// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package cn.kuaipan.kss.implement;


class _UploadProgress {

    _UploadProgress() {
        m_onUpload = null;
        m_isCanceled = false;
        m_transSize = 0;
    }

    public boolean TransBytes(int i) {
        if(i > 0) {
            m_transSize = i + m_transSize;
            if(m_onUpload != null)
                m_isCanceled = m_onUpload.OnTransData(m_transSize);
        }
        return m_isCanceled;
    }

    public boolean getIsCanceled() {
        return m_isCanceled;
    }

    public void init(cn.kuaipan.kss.KssUpload.RequestUploadInfo requestuploadinfo, cn.kuaipan.kss.KssDef.OnUpDownload onupdownload) {
        if(requestuploadinfo != null)
            m_onUpload = onupdownload;
    }

    public void setIsCanceled(boolean flag) {
        m_isCanceled = flag;
    }

    private boolean m_isCanceled;
    private cn.kuaipan.kss.KssDef.OnUpDownload m_onUpload;
    private int m_transSize;
}
