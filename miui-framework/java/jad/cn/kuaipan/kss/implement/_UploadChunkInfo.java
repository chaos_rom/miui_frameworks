// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package cn.kuaipan.kss.implement;

import org.json.JSONObject;

class _UploadChunkInfo {

    _UploadChunkInfo() {
        m_stateStr = null;
        m_uploadID = null;
        m_commitMeta = null;
    }

    private boolean needReRequestMetadata(String s) {
        boolean flag;
        String as[];
        int i;
        int j;
        flag = true;
        as = new String[4];
        as[0] = "ERR_INVALID_FILE_META";
        as[flag] = "ERR_INVALID_BLOCK_META";
        as[2] = "ERR_INVALID_UPLOAD_ID";
        as[3] = "ERR_BLOCK_CORRUPTED";
        i = as.length;
        j = 0;
_L3:
        if(j >= i)
            break MISSING_BLOCK_LABEL_60;
        if(!s.equals(as[j])) goto _L2; else goto _L1
_L1:
        return flag;
_L2:
        j++;
          goto _L3
        flag = false;
          goto _L1
    }

    public String getCommitMeta() {
        return m_commitMeta;
    }

    public String getUploadID() {
        return m_uploadID;
    }

    public cn.kuaipan.kss.KssDef.KssAPIResult parseInfo(String s) {
        cn.kuaipan.kss.KssDef.KssAPIResult kssapiresult = cn.kuaipan.kss.KssDef.KssAPIResult.OK;
label0:
        {
            JSONObject jsonobject = new JSONObject(s);
            m_stateStr = jsonobject.getString("stat");
            if(m_stateStr.equals("CONTINUE_UPLOAD")) {
                if(m_uploadID == null && jsonobject.has("upload_id"))
                    m_uploadID = jsonobject.getString("upload_id");
                if(m_uploadID == null)
                    kssapiresult = cn.kuaipan.kss.KssDef.KssAPIResult.Error;
                break label0;
            }
            Exception exception;
            if(m_stateStr.equals("BLOCK_COMPLETED")) {
                if(jsonobject.has("commit_meta"))
                    m_commitMeta = jsonobject.getString("commit_meta");
                break label0;
            }
            try {
                if(needReRequestMetadata(m_stateStr))
                    kssapiresult = cn.kuaipan.kss.KssDef.KssAPIResult.NeedRequest;
            }
            // Misplaced declaration of an exception variable
            catch(Exception exception) {
                exception.printStackTrace();
                kssapiresult = cn.kuaipan.kss.KssDef.KssAPIResult.Error;
            }
        }
        return kssapiresult;
    }

    public void reset() {
        m_stateStr = null;
        m_uploadID = null;
        m_commitMeta = null;
    }

    private String m_commitMeta;
    private String m_stateStr;
    private String m_uploadID;
}
