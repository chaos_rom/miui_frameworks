// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package cn.kuaipan.kss.implement;

import java.net.URI;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

public class HttpClientProxyHelper {

    public HttpClientProxyHelper() {
        m_proxyIndex = -1;
        m_ProxyURLs = null;
    }

    public HttpGet createGet(String s) {
        HttpGet httpget;
        if(!isUsingProxy()) {
            httpget = new HttpGet(s);
        } else {
            httpget = new HttpGet(getCurrentProxy());
            httpget.addHeader("Dest-Url", s);
        }
        return httpget;
    }

    public HttpGet createGet(URI uri) {
        HttpGet httpget;
        if(!isUsingProxy()) {
            httpget = new HttpGet(uri);
        } else {
            httpget = new HttpGet(getCurrentProxy());
            httpget.addHeader("Dest-Url", uri.toASCIIString());
        }
        return httpget;
    }

    public HttpPost createPost(URI uri) {
        HttpPost httppost;
        if(!isUsingProxy()) {
            httppost = new HttpPost(uri);
        } else {
            httppost = new HttpPost(getCurrentProxy());
            httppost.addHeader("Dest-Url", uri.toASCIIString());
        }
        return httppost;
    }

    public String getCurrentProxy() {
        String s;
        s = null;
        break MISSING_BLOCK_LABEL_2;
        if(m_ProxyURLs != null && m_proxyIndex >= 0 && m_proxyIndex < m_ProxyURLs.length)
            s = m_ProxyURLs[m_proxyIndex];
        return s;
    }

    public void init(String as[]) {
        m_ProxyURLs = as;
        m_proxyIndex = -1;
    }

    public boolean isUsingProxy() {
        boolean flag;
        if(m_proxyIndex > -1)
            flag = true;
        else
            flag = false;
        return flag;
    }

    public boolean nextProxy() {
        boolean flag = false;
        if(m_ProxyURLs != null) {
            m_proxyIndex = 1 + m_proxyIndex;
            if(m_proxyIndex < m_ProxyURLs.length)
                flag = true;
            else
                m_proxyIndex = -1 + m_proxyIndex;
        }
        return flag;
    }

    public boolean reset() {
        m_proxyIndex = -1;
        return true;
    }

    private String m_ProxyURLs[];
    private int m_proxyIndex;
}
