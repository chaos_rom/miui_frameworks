// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package cn.kuaipan.kss.implement;

import java.util.ArrayList;
import java.util.List;

class _UploadResult
    implements cn.kuaipan.kss.KssUpload.UploadResult {

    public _UploadResult() {
        m_Result = cn.kuaipan.kss.KssDef.KssAPIResult.OK;
        m_fileMeta = null;
        m_commitMetas = new ArrayList();
    }

    public void addCommitMeta(String s) {
        m_commitMetas.add(s);
    }

    public String getBlockCommitMeta(int i) {
        String s;
        if(i < 0 || i >= m_commitMetas.size())
            s = null;
        else
            s = (String)m_commitMetas.get(i);
        return s;
    }

    public int getBlockCount() {
        return m_commitMetas.size();
    }

    public String getFileMeta() {
        return m_fileMeta;
    }

    public cn.kuaipan.kss.KssDef.KssAPIResult getResult() {
        return m_Result;
    }

    public void setFileMeta(String s) {
        m_fileMeta = s;
    }

    private cn.kuaipan.kss.KssDef.KssAPIResult m_Result;
    private List m_commitMetas;
    private String m_fileMeta;
}
