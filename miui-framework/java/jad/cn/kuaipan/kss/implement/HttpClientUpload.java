// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package cn.kuaipan.kss.implement;

import cn.kuaipan.kss.KssDef;
import cn.kuaipan.kss.KssUpload;
import cn.kuaipan.kss.utils.RC4;
import java.io.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.CRC32;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.message.BasicNameValuePair;

// Referenced classes of package cn.kuaipan.kss.implement:
//            _UploadProgress, _UploadResult, HttpClientProxyHelper, _UploadChunkInfo

public class HttpClientUpload
    implements KssUpload {

    public HttpClientUpload(HttpClient httpclient, HttpClientProxyHelper httpclientproxyhelper) {
        CHUNKSIZE = KssDef.CHUNKSIZE_MIN;
        m_client = null;
        m_proxyHelper = null;
        m_requestInfo = null;
        m_progress = null;
        m_uploadResult = null;
        m_chunkDataBuffer = new byte[CHUNKSIZE];
        m_rc4 = new RC4();
        m_crc32 = new CRC32();
        m_client = httpclient;
        m_proxyHelper = httpclientproxyhelper;
        m_progress = new _UploadProgress();
        m_uploadResult = new _UploadResult();
    }

    private HttpPost _genUploadChunkPost(URI uri, byte abyte0[], int i) {
        HttpPost httppost;
        Object obj;
        if(m_proxyHelper == null)
            httppost = new HttpPost(uri);
        else
            httppost = m_proxyHelper.createPost(uri);
        if(i == abyte0.length)
            obj = new ByteArrayEntity(abyte0);
        else
            obj = new InputStreamEntity(new ByteArrayInputStream(abyte0, 0, i), i);
        httppost.setEntity(((HttpEntity) (obj)));
        return httppost;
    }

    private URI _genUploadChunkURI(String s, int i, int j, _UploadChunkInfo _puploadchunkinfo) throws Exception {
        ArrayList arraylist = new ArrayList();
        arraylist.add(new BasicNameValuePair("chunk_pos", Integer.toString(i)));
        arraylist.add(new BasicNameValuePair("body_sum", s));
        if(i == 0) {
            arraylist.add(new BasicNameValuePair("file_meta", m_requestInfo.getFileMeta()));
            arraylist.add(new BasicNameValuePair("block_meta", m_requestInfo.getBlockMeta(j)));
        } else {
            arraylist.add(new BasicNameValuePair("upload_id", _puploadchunkinfo.getUploadID()));
        }
        return URIUtils.createURI(m_requestInfo.getProtocol(), m_requestInfo.getNodeIP(0), -1, "upload_block_chunk", URLEncodedUtils.format(arraylist, "UTF-8"), null);
    }

    private cn.kuaipan.kss.KssDef.KssAPIResult _parseResponse(HttpResponse httpresponse, _UploadChunkInfo _puploadchunkinfo) {
        cn.kuaipan.kss.KssDef.KssAPIResult kssapiresult;
        BufferedReader bufferedreader;
        StringBuffer stringbuffer;
        kssapiresult = cn.kuaipan.kss.KssDef.KssAPIResult.OK;
        bufferedreader = null;
        stringbuffer = new StringBuffer();
        BufferedReader bufferedreader1 = new BufferedReader(new InputStreamReader(httpresponse.getEntity().getContent()));
_L4:
        String s = bufferedreader1.readLine();
        if(s == null) goto _L2; else goto _L1
_L1:
        stringbuffer.append(s);
        if(true) goto _L4; else goto _L3
_L3:
        IllegalStateException illegalstateexception;
        illegalstateexception;
        bufferedreader = bufferedreader1;
_L10:
        illegalstateexception.printStackTrace();
        kssapiresult = cn.kuaipan.kss.KssDef.KssAPIResult.Error;
        if(bufferedreader != null)
            try {
                bufferedreader.close();
            }
            catch(Exception exception3) {
                exception3.printStackTrace();
            }
_L5:
        if(kssapiresult == cn.kuaipan.kss.KssDef.KssAPIResult.OK)
            kssapiresult = _puploadchunkinfo.parseInfo(stringbuffer.toString());
        return kssapiresult;
_L2:
        if(bufferedreader1 != null)
            try {
                bufferedreader1.close();
            }
            catch(Exception exception4) {
                exception4.printStackTrace();
            }
        break MISSING_BLOCK_LABEL_91;
        IOException ioexception1;
        ioexception1;
_L8:
        kssapiresult = cn.kuaipan.kss.KssDef.KssAPIResult.NetTimeout;
        if(bufferedreader != null)
            try {
                bufferedreader.close();
            }
            catch(Exception exception2) {
                exception2.printStackTrace();
            }
          goto _L5
        Exception exception;
        exception;
_L7:
        if(bufferedreader != null)
            try {
                bufferedreader.close();
            }
            catch(Exception exception1) {
                exception1.printStackTrace();
            }
        throw exception;
        exception;
        bufferedreader = bufferedreader1;
        if(true) goto _L7; else goto _L6
_L6:
        IOException ioexception;
        ioexception;
        bufferedreader = bufferedreader1;
          goto _L8
        illegalstateexception;
        if(true) goto _L10; else goto _L9
_L9:
    }

    private int _readChunkData(InputStream inputstream, byte abyte0[]) throws IOException {
        int i = 0;
        do {
            if(i >= CHUNKSIZE)
                break;
            int j = inputstream.read(abyte0, i, CHUNKSIZE - i);
            if(j < 0)
                break;
            i += j;
        } while(true);
        return i;
    }

    private cn.kuaipan.kss.KssDef.KssAPIResult _upload(InputStream inputstream) throws Exception {
        int i;
        int j;
        i = m_requestInfo.getBlockCount();
        j = 0;
_L7:
        if(j >= i) goto _L2; else goto _L1
_L1:
        if(!m_requestInfo.getBlockIsExist(j)) goto _L4; else goto _L3
_L3:
        cn.kuaipan.kss.KssDef.KssAPIResult kssapiresult;
        inputstream.skip(KssDef.BLOCKSIZE);
        m_uploadResult.addCommitMeta(m_requestInfo.getBlockMeta(j));
        if(m_progress.TransBytes(KssDef.BLOCKSIZE))
            continue; /* Loop/switch isn't completed */
        kssapiresult = cn.kuaipan.kss.KssDef.KssAPIResult.Cancel;
_L6:
        return kssapiresult;
_L4:
        _UploadChunkInfo _luploadchunkinfo;
        _luploadchunkinfo = new _UploadChunkInfo();
        kssapiresult = _uploadBlock(inputstream, j, _luploadchunkinfo);
        if(kssapiresult != cn.kuaipan.kss.KssDef.KssAPIResult.OK) goto _L6; else goto _L5
_L5:
        m_uploadResult.addCommitMeta(_luploadchunkinfo.getCommitMeta());
        j++;
          goto _L7
_L2:
        kssapiresult = cn.kuaipan.kss.KssDef.KssAPIResult.OK;
          goto _L6
    }

    private cn.kuaipan.kss.KssDef.KssAPIResult _uploadBlock(InputStream inputstream, int i, _UploadChunkInfo _puploadchunkinfo) throws Exception {
        cn.kuaipan.kss.KssDef.KssAPIResult kssapiresult;
        int j;
        kssapiresult = cn.kuaipan.kss.KssDef.KssAPIResult.OK;
        j = 0;
_L5:
        int l = _readChunkData(inputstream, m_chunkDataBuffer);
        int k = l;
_L3:
        if(k != 0) goto _L2; else goto _L1
_L1:
        return kssapiresult;
        IOException ioexception;
        ioexception;
        kssapiresult = cn.kuaipan.kss.KssDef.KssAPIResult.Error;
        k = 0;
          goto _L3
_L2:
        m_rc4.makeKey(m_requestInfo.getSeureKey());
        m_rc4.genRC4(m_chunkDataBuffer, 0, k, m_chunkDataBuffer, 0);
        boolean flag = false;
        do {
            kssapiresult = _uploadChunk(i, j, k, _puploadchunkinfo);
            if(cn.kuaipan.kss.KssDef.KssAPIResult.NetTimeout == kssapiresult)
                if(m_proxyHelper != null && m_proxyHelper.nextProxy())
                    flag = true;
                else
                    flag = false;
        } while(flag);
        if(cn.kuaipan.kss.KssDef.KssAPIResult.OK != kssapiresult) goto _L1; else goto _L4
_L4:
        j += k;
        if(j < KssDef.BLOCKSIZE) goto _L5; else goto _L1
    }

    private cn.kuaipan.kss.KssDef.KssAPIResult _uploadChunk(int i, int j, int k, _UploadChunkInfo _puploadchunkinfo) throws Exception {
        HttpPost httppost;
        int l;
        cn.kuaipan.kss.KssDef.KssAPIResult.OK;
        httppost = null;
        m_crc32.reset();
        m_crc32.update(m_chunkDataBuffer, 0, k);
        l = (int)m_crc32.getValue();
        URI uri;
        HttpResponse httpresponse;
        int i1;
        uri = _genUploadChunkURI(Integer.toString(l), j, i, _puploadchunkinfo);
        httpresponse = null;
        i1 = 0;
_L2:
        HttpPost httppost1;
        if(i1 >= KssDef.NET_RETRY_TIMES)
            break; /* Loop/switch isn't completed */
        httppost1 = _genUploadChunkPost(uri, m_chunkDataBuffer, k);
        httppost = httppost1;
        HttpResponse httpresponse1 = m_client.execute(httppost);
        httpresponse = httpresponse1;
_L3:
        i1++;
        if(httpresponse == null) goto _L2; else goto _L1
_L1:
        if(httpresponse != null)
            break MISSING_BLOCK_LABEL_164;
        cn.kuaipan.kss.KssDef.KssAPIResult kssapiresult = cn.kuaipan.kss.KssDef.KssAPIResult.NetTimeout;
_L4:
        httppost.abort();
        return kssapiresult;
        ClientProtocolException clientprotocolexception;
        clientprotocolexception;
        throw clientprotocolexception;
        Exception exception1;
        exception1;
        throw exception1;
        Exception exception;
        exception;
        httppost.abort();
        throw exception;
        IOException ioexception;
        ioexception;
        httppost.abort();
        httpresponse = null;
          goto _L3
        Exception exception2;
        exception2;
        throw exception2;
        kssapiresult = _parseResponse(httpresponse, _puploadchunkinfo);
        if(kssapiresult == cn.kuaipan.kss.KssDef.KssAPIResult.OK) {
            j + k;
            if(!m_progress.TransBytes(k))
                kssapiresult = cn.kuaipan.kss.KssDef.KssAPIResult.Cancel;
        }
          goto _L4
    }

    public cn.kuaipan.kss.KssUpload.UploadResult getCommitInfo() throws Exception {
        return m_uploadResult;
    }

    public boolean init(cn.kuaipan.kss.KssUpload.RequestUploadInfo requestuploadinfo, cn.kuaipan.kss.KssDef.OnUpDownload onupdownload, cn.kuaipan.kss.KssDef.NetState netstate) {
        boolean flag;
        if(requestuploadinfo == null) {
            flag = false;
        } else {
            m_requestInfo = requestuploadinfo;
            m_progress.init(requestuploadinfo, onupdownload);
            m_uploadResult.setFileMeta(m_requestInfo.getFileMeta());
            flag = true;
        }
        return flag;
    }

    public void terminal() {
        m_client = null;
        m_requestInfo = null;
        m_uploadResult = null;
        m_progress = null;
        m_proxyHelper = null;
        m_crc32 = null;
        m_rc4 = null;
        m_chunkDataBuffer = null;
    }

    public cn.kuaipan.kss.KssDef.KssAPIResult upload(InputStream inputstream) throws Exception {
        return _upload(inputstream);
    }

    public cn.kuaipan.kss.KssDef.KssAPIResult upload(byte abyte0[]) throws Exception {
        return _upload(new ByteArrayInputStream(abyte0));
    }

    private int CHUNKSIZE;
    private byte m_chunkDataBuffer[];
    private HttpClient m_client;
    private CRC32 m_crc32;
    private _UploadProgress m_progress;
    private HttpClientProxyHelper m_proxyHelper;
    private RC4 m_rc4;
    private cn.kuaipan.kss.KssUpload.RequestUploadInfo m_requestInfo;
    private _UploadResult m_uploadResult;
}
