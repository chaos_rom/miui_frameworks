// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package cn.kuaipan.kss.implement;

import cn.kuaipan.kss.KssDef;
import cn.kuaipan.kss.KssDownload;
import cn.kuaipan.kss.utils.Encode;
import cn.kuaipan.kss.utils.RC4;
import java.io.*;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

// Referenced classes of package cn.kuaipan.kss.implement:
//            _DownloadProgress, HttpClientProxyHelper, KPDownloadTransControl

public class HttpClientDownload
    implements KssDownload {

    public HttpClientDownload(HttpClient httpclient, HttpClientProxyHelper httpclientproxyhelper) {
        m_client = null;
        m_dataBuffer = null;
        m_proxyHelper = null;
        m_downloadInfo = null;
        m_downProgress = null;
        m_currentBlockPos = 0;
        m_netState = cn.kuaipan.kss.KssDef.NetState.Wifi;
        m_maxRetryCount = 0;
        m_client = httpclient;
        m_proxyHelper = httpclientproxyhelper;
        m_downProgress = new _DownloadProgress();
        m_dataBuffer = new byte[KssDef.DOWNLOAD_BUFFER];
    }

    private cn.kuaipan.kss.KssDef.KssAPIResult _downloadBlock(cn.kuaipan.kss.KssDownload.DownloadTransControl downloadtranscontrol, cn.kuaipan.kss.KssDownload.BlockDownloadInfo blockdownloadinfo) throws Exception {
        RC4 rc4;
        byte abyte0[];
        cn.kuaipan.kss.KssDef.KssAPIResult kssapiresult;
        int i;
        rc4 = new RC4();
        abyte0 = Encode.hexStringToByteArray(m_downloadInfo.getSecureKey());
        kssapiresult = cn.kuaipan.kss.KssDef.KssAPIResult.Error;
        i = 0;
_L11:
        if(i >= m_maxRetryCount) goto _L2; else goto _L1
_L1:
        HttpGet httpget = null;
        if(m_proxyHelper != null) goto _L4; else goto _L3
_L3:
        httpget = new HttpGet(blockdownloadinfo.getBlockDownload_FullURL(0));
_L9:
        InputStream inputstream;
        httpget.addHeader("Range", (new StringBuilder()).append("bytes=").append(Integer.toString(m_currentBlockPos)).append("-").toString());
        rc4.makeKey(abyte0);
        inputstream = m_client.execute(httpget).getEntity().getContent();
        kssapiresult = cn.kuaipan.kss.KssDef.KssAPIResult.OK;
_L8:
        int j = inputstream.read(m_dataBuffer, 0, KssDef.DOWNLOAD_BUFFER);
        if(j < 0) goto _L6; else goto _L5
_L5:
        rc4.genRC4(m_dataBuffer, 0, j, m_dataBuffer, 0);
        downloadtranscontrol.addData(m_dataBuffer, j);
        m_currentBlockPos = j + m_currentBlockPos;
        m_downProgress.TransBytes(j);
        if(!m_downProgress.getIsCanceled()) goto _L8; else goto _L7
_L7:
        httpget.abort();
        kssapiresult = cn.kuaipan.kss.KssDef.KssAPIResult.Cancel;
_L6:
        if(kssapiresult == cn.kuaipan.kss.KssDef.KssAPIResult.NetTimeout)
            break MISSING_BLOCK_LABEL_315;
_L2:
        return kssapiresult;
_L4:
        httpget = m_proxyHelper.createGet(blockdownloadinfo.getBlockDownload_FullURL(0));
          goto _L9
        IOException ioexception1;
        ioexception1;
        ioexception1.printStackTrace();
        kssapiresult = cn.kuaipan.kss.KssDef.KssAPIResult.Error;
          goto _L6
        IOException ioexception;
        ioexception;
        httpget.abort();
        kssapiresult = cn.kuaipan.kss.KssDef.KssAPIResult.NetTimeout;
          goto _L6
        RuntimeException runtimeexception;
        runtimeexception;
        httpget.abort();
        kssapiresult = cn.kuaipan.kss.KssDef.KssAPIResult.Error;
          goto _L6
        Exception exception1;
        exception1;
        kssapiresult = cn.kuaipan.kss.KssDef.KssAPIResult.Error;
          goto _L6
        Exception exception;
        exception;
        throw exception;
        i++;
        if(true) goto _L11; else goto _L10
_L10:
    }

    public cn.kuaipan.kss.KssDef.KssAPIResult download(cn.kuaipan.kss.KssDownload.DownloadTransControl downloadtranscontrol) throws Exception {
        m_downProgress.startTrans(downloadtranscontrol);
        int i = downloadtranscontrol.getStartPos() / KssDef.BLOCKSIZE;
        m_currentBlockPos = downloadtranscontrol.getStartPos() % KssDef.BLOCKSIZE;
        cn.kuaipan.kss.KssDef.KssAPIResult kssapiresult = cn.kuaipan.kss.KssDef.KssAPIResult.Error;
        do {
            if(i >= m_downloadInfo.getBlockCount())
                break;
            cn.kuaipan.kss.KssDownload.BlockDownloadInfo blockdownloadinfo = m_downloadInfo.getBlockInfos(i);
            for(boolean flag = true; flag;) {
                kssapiresult = _downloadBlock(downloadtranscontrol, blockdownloadinfo);
                if(kssapiresult == cn.kuaipan.kss.KssDef.KssAPIResult.NetTimeout) {
                    if(m_proxyHelper != null && m_proxyHelper.nextProxy())
                        flag = true;
                    else
                        flag = false;
                } else {
                    flag = false;
                }
            }

            if(kssapiresult != cn.kuaipan.kss.KssDef.KssAPIResult.OK)
                break;
            m_currentBlockPos = 0;
            i++;
        } while(true);
        if(kssapiresult == cn.kuaipan.kss.KssDef.KssAPIResult.OK)
            downloadtranscontrol.setEndState(cn.kuaipan.kss.KssDownload.DownloadTransControl.EndState.Completed);
        else
            downloadtranscontrol.setEndState(cn.kuaipan.kss.KssDownload.DownloadTransControl.EndState.Interrupt);
        return kssapiresult;
    }

    public cn.kuaipan.kss.KssDef.KssAPIResult download(OutputStream outputstream, int i) throws Exception {
        if(i < 0)
            i = 0;
        KPDownloadTransControl kpdownloadtranscontrol = new KPDownloadTransControl();
        kpdownloadtranscontrol.init(outputstream);
        kpdownloadtranscontrol.setStartPos(i);
        return download(((cn.kuaipan.kss.KssDownload.DownloadTransControl) (kpdownloadtranscontrol)));
    }

    public boolean init(cn.kuaipan.kss.KssDownload.RequestDownloadInfo requestdownloadinfo, cn.kuaipan.kss.KssDef.OnUpDownload onupdownload, cn.kuaipan.kss.KssDef.NetState netstate) {
        m_downloadInfo = requestdownloadinfo;
        m_downProgress.init(requestdownloadinfo, onupdownload);
        m_netState = netstate;
        m_maxRetryCount = KssDef.NET_RETRY_TIMES;
        if(m_netState != cn.kuaipan.kss.KssDef.NetState.Wifi)
            m_maxRetryCount = 2 * KssDef.NET_RETRY_TIMES;
        return true;
    }

    public void terminal() {
        m_client = null;
        m_dataBuffer = null;
        m_proxyHelper = null;
        m_downloadInfo = null;
        m_downProgress = null;
    }

    private HttpClient m_client;
    private int m_currentBlockPos;
    private byte m_dataBuffer[];
    private _DownloadProgress m_downProgress;
    private cn.kuaipan.kss.KssDownload.RequestDownloadInfo m_downloadInfo;
    private int m_maxRetryCount;
    private cn.kuaipan.kss.KssDef.NetState m_netState;
    private HttpClientProxyHelper m_proxyHelper;
}
