// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package cn.kuaipan.kss;


public class KssDef {
    public static interface OnUpDownload {

        public abstract boolean OnTransData(int i);

        public abstract boolean OnTransFail(int i);
    }

    public static final class NetState extends Enum {

        public static NetState valueOf(String s) {
            return (NetState)Enum.valueOf(cn/kuaipan/kss/KssDef$NetState, s);
        }

        public static NetState[] values() {
            return (NetState[])$VALUES.clone();
        }

        private static final NetState $VALUES[];
        public static final NetState MN2G;
        public static final NetState MN3G;
        public static final NetState Wifi;

        static  {
            Wifi = new NetState("Wifi", 0);
            MN3G = new NetState("MN3G", 1);
            MN2G = new NetState("MN2G", 2);
            NetState anetstate[] = new NetState[3];
            anetstate[0] = Wifi;
            anetstate[1] = MN3G;
            anetstate[2] = MN2G;
            $VALUES = anetstate;
        }

        private NetState(String s, int i) {
            super(s, i);
        }
    }

    public static final class KssAPIResult extends Enum {

        public static KssAPIResult valueOf(String s) {
            return (KssAPIResult)Enum.valueOf(cn/kuaipan/kss/KssDef$KssAPIResult, s);
        }

        public static KssAPIResult[] values() {
            return (KssAPIResult[])$VALUES.clone();
        }

        private static final KssAPIResult $VALUES[];
        public static final KssAPIResult Cancel;
        public static final KssAPIResult DataCorrupted;
        public static final KssAPIResult Error;
        public static final KssAPIResult NeedRequest;
        public static final KssAPIResult NetTimeout;
        public static final KssAPIResult OK;
        public static final KssAPIResult ServerDenyReadOnly;
        public static final KssAPIResult SpaceOver;

        static  {
            OK = new KssAPIResult("OK", 0);
            Error = new KssAPIResult("Error", 1);
            Cancel = new KssAPIResult("Cancel", 2);
            NetTimeout = new KssAPIResult("NetTimeout", 3);
            NeedRequest = new KssAPIResult("NeedRequest", 4);
            DataCorrupted = new KssAPIResult("DataCorrupted", 5);
            SpaceOver = new KssAPIResult("SpaceOver", 6);
            ServerDenyReadOnly = new KssAPIResult("ServerDenyReadOnly", 7);
            KssAPIResult akssapiresult[] = new KssAPIResult[8];
            akssapiresult[0] = OK;
            akssapiresult[1] = Error;
            akssapiresult[2] = Cancel;
            akssapiresult[3] = NetTimeout;
            akssapiresult[4] = NeedRequest;
            akssapiresult[5] = DataCorrupted;
            akssapiresult[6] = SpaceOver;
            akssapiresult[7] = ServerDenyReadOnly;
            $VALUES = akssapiresult;
        }

        private KssAPIResult(String s, int i) {
            super(s, i);
        }
    }


    public KssDef() {
    }

    public static int BLOCKSIZE = 0;
    public static int CHUNKSIZE_MIN = 0;
    public static int DOWNLOAD_BUFFER = 0;
    public static final String FUNC_UPLOADBLOCKCHUNK = "upload_block_chunk";
    public static final String KEY_BLOCKINFO = "block_infos";
    public static final String KEY_BLOCKMETA = "block_meta";
    public static final String KEY_BLOCKMETAS = "block_metas";
    public static final String KEY_BLOCKS = "blocks";
    public static final String KEY_BODYSUM = "body_sum";
    public static final String KEY_CHUNKPOS = "chunk_pos";
    public static final String KEY_COMMITMETA = "commit_meta";
    public static final String KEY_COMMITMETAS = "commit_metas";
    public static final String KEY_DESTURL = "Dest-Url";
    public static final String KEY_FILEMETA = "file_meta";
    public static final String KEY_ISEXISTED = "is_existed";
    public static final String KEY_MD5 = "md5";
    public static final String KEY_NODEURLS = "node_urls";
    public static final String KEY_PROXIES = "proxies";
    public static final String KEY_SECUREKEY = "secure_key";
    public static final String KEY_SHA1 = "sha1";
    public static final String KEY_SIZE = "size";
    public static final String KEY_STAT = "stat";
    public static final String KEY_STOID = "stoid";
    public static final String KEY_UPLOADID = "upload_id";
    public static final String KEY_URL = "url";
    public static final String KEY_URLS = "urls";
    public static int NET_RETRY_TIMES = 0;
    public static final String VALUE_BLOCKCOMPLETED = "BLOCK_COMPLETED";
    public static final String VALUE_CONTINUEUPLOAD = "CONTINUE_UPLOAD";
    public static final String VALUE_FILEEXISTED = "FILE_EXISTED";
    public static final String VALUE_OK = "OK";

    static  {
        BLOCKSIZE = 0x400000;
        CHUNKSIZE_MIN = 0x10000;
        DOWNLOAD_BUFFER = 8192;
        NET_RETRY_TIMES = 3;
    }
}
