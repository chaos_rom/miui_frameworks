// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package cn.kuaipan.kss.utils;

import java.security.InvalidKeyException;

public class RC4 {

    public RC4() {
    }

    public void genRC4(byte abyte0[], int i, int j, byte abyte1[], int k) throws InvalidKeyException {
        int l = 0;
        int i1 = k;
        int j2;
        for(int j1 = i; l < j; j1 = j2) {
            x = 0xff & 1 + x;
            y = 0xff & sBox[x] + y;
            int k1 = sBox[x];
            sBox[x] = sBox[y];
            sBox[y] = k1;
            int l1 = 0xff & sBox[x] + sBox[y];
            int i2 = i1 + 1;
            j2 = j1 + 1;
            abyte1[i1] = (byte)(abyte0[j1] ^ sBox[l1]);
            l++;
            i1 = i2;
        }

    }

    public void makeKey(byte abyte0[]) throws InvalidKeyException {
        if(abyte0 == null)
            throw new InvalidKeyException("Null user key");
        int i = abyte0.length;
        if(i == 0)
            throw new InvalidKeyException("Invalid user key length");
        x = 0;
        y = 0;
        for(int j = 0; j < 256; j++)
            sBox[j] = j;

        int k = 0;
        int l = 0;
        for(int i1 = 0; i1 < 256; i1++) {
            l = 0xff & l + ((0xff & abyte0[k]) + sBox[i1]);
            int j1 = sBox[i1];
            sBox[i1] = sBox[l];
            sBox[l] = j1;
            k = (k + 1) % i;
        }

    }

    private final int sBox[] = new int[256];
    private int x;
    private int y;
}
