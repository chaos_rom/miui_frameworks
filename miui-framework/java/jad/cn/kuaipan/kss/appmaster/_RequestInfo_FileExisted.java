// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package cn.kuaipan.kss.appmaster;


class _RequestInfo_FileExisted
    implements cn.kuaipan.kss.KssUpload.RequestUploadInfo {

    _RequestInfo_FileExisted() {
    }

    public static cn.kuaipan.kss.KssUpload.RequestUploadInfo getInstance() {
        if(requestInfo == null)
            requestInfo = new _RequestInfo_FileExisted();
        return requestInfo;
    }

    public int getBlockCount() {
        return 0;
    }

    public boolean getBlockIsExist(int i) {
        return false;
    }

    public String getBlockMeta(int i) {
        return null;
    }

    public String getFileMeta() {
        return null;
    }

    public int getFileSize() {
        return 0;
    }

    public String getFileStoreID() {
        return null;
    }

    public String getNodeIP(int i) {
        return null;
    }

    public int getNodeIPCount() {
        return 0;
    }

    public String getProtocol() {
        return null;
    }

    public cn.kuaipan.kss.KssUpload.RequestUploadInfo.RequestUploadState getRequestUploadState() {
        return cn.kuaipan.kss.KssUpload.RequestUploadInfo.RequestUploadState.FileExisted;
    }

    public cn.kuaipan.kss.KssDef.KssAPIResult getResult() {
        return cn.kuaipan.kss.KssDef.KssAPIResult.OK;
    }

    public byte[] getSeureKey() {
        return null;
    }

    static cn.kuaipan.kss.KssUpload.RequestUploadInfo requestInfo = null;

}
