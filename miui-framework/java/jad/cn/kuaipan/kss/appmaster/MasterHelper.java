// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package cn.kuaipan.kss.appmaster;

import cn.kuaipan.kss.KssDef;
import cn.kuaipan.kss.utils.Encode;
import java.io.InputStream;
import java.security.MessageDigest;
import org.json.JSONArray;
import org.json.JSONObject;

public class MasterHelper {

    public MasterHelper() {
    }

    public static JSONObject genCommiteUploadJson(cn.kuaipan.kss.KssUpload.UploadResult uploadresult) throws Exception {
        JSONObject jsonobject = new JSONObject();
        jsonobject.put("file_meta", uploadresult.getFileMeta());
        JSONArray jsonarray = new JSONArray();
        for(int i = 0; i < uploadresult.getBlockCount(); i++)
            jsonarray.put((new JSONObject()).put("commit_meta", uploadresult.getBlockCommitMeta(i)));

        jsonobject.put("commit_metas", jsonarray);
        return jsonobject;
    }

    public static JSONObject genRequestUploadJson(InputStream inputstream) throws Exception {
        JSONArray jsonarray = new JSONArray();
        MessageDigest messagedigest = MessageDigest.getInstance("SHA1");
        MessageDigest messagedigest1 = MessageDigest.getInstance("MD5");
        byte abyte0[] = new byte[KssDef.CHUNKSIZE_MIN];
        boolean flag = true;
        do {
            int i;
label0:
            {
label1:
                {
label2:
                    {
                        if(!flag)
                            break label1;
                        i = 0;
                        do {
                            if(i >= KssDef.BLOCKSIZE)
                                break label2;
                            int j = inputstream.read(abyte0, 0, Math.min(abyte0.length, KssDef.BLOCKSIZE - i));
                            if(j < 0)
                                break;
                            messagedigest.update(abyte0, 0, j);
                            messagedigest1.update(abyte0, 0, j);
                            i += j;
                        } while(true);
                        flag = false;
                    }
                    if(i != 0)
                        break label0;
                }
                JSONObject jsonobject = new JSONObject();
                jsonobject.put("block_infos", jsonarray);
                return jsonobject;
            }
            JSONObject jsonobject1 = new JSONObject();
            jsonobject1.put("sha1", Encode.byteArrayToHexString(messagedigest.digest()));
            jsonobject1.put("md5", Encode.byteArrayToHexString(messagedigest1.digest()));
            jsonobject1.put("size", i);
            jsonarray.put(jsonobject1);
        } while(true);
    }

    public static String[] parseKssProxiesInfo(String s) {
        String as[];
        JSONArray jsonarray;
        int i;
        JSONObject jsonobject = new JSONObject(s);
        if(!jsonobject.get("stat").equals("OK")) {
            as = null;
            break MISSING_BLOCK_LABEL_80;
        }
        jsonarray = jsonobject.getJSONArray("proxies");
        as = new String[jsonarray.length()];
        i = 0;
_L1:
        if(i >= as.length)
            break MISSING_BLOCK_LABEL_80;
        as[i] = jsonarray.getJSONObject(i).getString("url");
        i++;
          goto _L1
        Exception exception;
        exception;
        as = null;
        return as;
    }
}
