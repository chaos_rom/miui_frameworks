// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package cn.kuaipan.kss.appmaster;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

// Referenced classes of package cn.kuaipan.kss.appmaster:
//            _BlockInfo

public class RequestDownloadParse
    implements cn.kuaipan.kss.KssDownload.RequestDownloadInfo {

    public RequestDownloadParse() {
        m_Result = cn.kuaipan.kss.KssDef.KssAPIResult.Error;
        m_secureKey = null;
        m_fileSize = 0;
        m_blockInfos = new ArrayList();
    }

    public int getBlockCount() {
        return m_blockInfos.size();
    }

    public cn.kuaipan.kss.KssDownload.BlockDownloadInfo getBlockInfos(int i) {
        cn.kuaipan.kss.KssDownload.BlockDownloadInfo blockdownloadinfo;
        if(i < 0 || i >= m_blockInfos.size())
            blockdownloadinfo = null;
        else
            blockdownloadinfo = (cn.kuaipan.kss.KssDownload.BlockDownloadInfo)m_blockInfos.get(i);
        return blockdownloadinfo;
    }

    public int getFileSize() {
        return m_fileSize;
    }

    public cn.kuaipan.kss.KssDef.KssAPIResult getResult() {
        return m_Result;
    }

    public String getSecureKey() {
        return m_secureKey;
    }

    public boolean parseRequestDownloadInfo(String s) {
        boolean flag1 = parseRequestDownloadInfo(new JSONObject(s));
        boolean flag = flag1;
_L2:
        return flag;
        Exception exception;
        exception;
        exception.printStackTrace();
        flag = false;
        if(true) goto _L2; else goto _L1
_L1:
    }

    public boolean parseRequestDownloadInfo(JSONObject jsonobject) {
        boolean flag;
        flag = false;
        m_fileSize = 0;
        JSONArray jsonarray;
        int i;
        if(!jsonobject.get("stat").equals("OK"))
            break MISSING_BLOCK_LABEL_133;
        m_Result = cn.kuaipan.kss.KssDef.KssAPIResult.OK;
        m_secureKey = jsonobject.getString("secure_key");
        jsonarray = jsonobject.getJSONArray("blocks");
        i = 0;
_L1:
        if(i >= jsonarray.length())
            break MISSING_BLOCK_LABEL_131;
        JSONObject jsonobject1 = jsonarray.getJSONObject(i);
        _BlockInfo _lblockinfo = new _BlockInfo(i);
        _lblockinfo.parseBlockInfo(jsonobject1);
        m_blockInfos.add(_lblockinfo);
        m_fileSize = m_fileSize + _lblockinfo.getBlockSize();
        i++;
          goto _L1
        Exception exception;
        exception;
        m_fileSize = 0;
        break MISSING_BLOCK_LABEL_133;
        flag = true;
        return flag;
    }

    private cn.kuaipan.kss.KssDef.KssAPIResult m_Result;
    private List m_blockInfos;
    private int m_fileSize;
    private String m_secureKey;
}
