// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package cn.kuaipan.kss.appmaster;

import cn.kuaipan.kss.utils.Encode;
import java.io.PrintStream;
import org.json.*;

// Referenced classes of package cn.kuaipan.kss.appmaster:
//            _RequestInfo_FileExisted

public class RequestUploadParse
    implements cn.kuaipan.kss.KssUpload.RequestUploadInfo {

    public RequestUploadParse() {
        m_uploadState = cn.kuaipan.kss.KssUpload.RequestUploadInfo.RequestUploadState.NeedUpload;
        m_fileStoreID = null;
        m_nodeIPs = null;
        m_protocolsStr = null;
        m_secureKey = null;
        m_fileMeta = null;
        m_blockMeta = null;
        m_blockIsExist = null;
        m_dataSize = 0;
    }

    private void _clear() {
        m_nodeIPs = null;
        m_protocolsStr = null;
        m_secureKey = null;
        m_fileMeta = null;
        m_blockMeta = null;
        m_blockIsExist = null;
        m_dataSize = 0;
    }

    public static cn.kuaipan.kss.KssUpload.RequestUploadInfo getFileExitRequestInfo() {
        return _RequestInfo_FileExisted.getInstance();
    }

    private boolean parseInfo(JSONObject jsonobject) {
        if(!jsonobject.getString("stat").equals("FILE_EXISTED")) goto _L2; else goto _L1
_L1:
        boolean flag;
        m_uploadState = cn.kuaipan.kss.KssUpload.RequestUploadInfo.RequestUploadState.FileExisted;
        m_fileStoreID = jsonobject.getString("stoid");
        flag = true;
          goto _L3
_L2:
        JSONArray jsonarray;
        m_uploadState = cn.kuaipan.kss.KssUpload.RequestUploadInfo.RequestUploadState.NeedUpload;
        m_fileStoreID = "";
        jsonarray = jsonobject.getJSONArray("node_urls");
        if(jsonarray.length() >= 1) goto _L5; else goto _L4
_L4:
        flag = false;
          goto _L3
_L5:
        JSONArray jsonarray1;
        int j;
        m_nodeIPs = new String[jsonarray.length()];
        for(int i = 0; i < jsonarray.length(); i++) {
            String s = jsonarray.getString(i);
            int l = s.indexOf("://");
            if(m_protocolsStr == null && l > 0)
                m_protocolsStr = s.substring(0, l);
            m_nodeIPs[i] = s.substring(l + 3, s.length());
        }

        m_secureKey = Encode.hexStringToByteArray(jsonobject.getString("secure_key"));
        m_fileMeta = jsonobject.getString("file_meta");
        jsonarray1 = jsonobject.getJSONArray("block_metas");
        m_blockMeta = new String[jsonarray1.length()];
        m_blockIsExist = new boolean[jsonarray1.length()];
        j = 0;
_L9:
        if(j >= jsonarray1.length()) goto _L7; else goto _L6
_L6:
        JSONObject jsonobject1;
        boolean aflag[];
        boolean flag1;
        jsonobject1 = jsonarray1.getJSONObject(j);
        int k = jsonobject1.getInt("is_existed");
        aflag = m_blockIsExist;
        if(k != 0)
            break MISSING_BLOCK_LABEL_330;
        flag1 = false;
_L10:
        aflag[j] = flag1;
        if(flag1)
            m_blockMeta[j] = jsonobject1.getString("commit_meta");
        else
            m_blockMeta[j] = jsonobject1.getString("block_meta");
          goto _L8
        JSONException jsonexception;
        jsonexception;
        jsonexception.printStackTrace();
        flag = false;
          goto _L3
_L7:
        flag = true;
_L3:
        return flag;
_L8:
        j++;
          goto _L9
        flag1 = true;
          goto _L10
    }

    public int getBlockCount() {
        int i;
        if(m_blockMeta == null)
            i = 0;
        else
            i = m_blockMeta.length;
        return i;
    }

    public boolean getBlockIsExist(int i) {
        boolean flag;
        if(m_blockMeta != null && i >= 0 && i < m_blockMeta.length)
            flag = m_blockIsExist[i];
        else
            flag = false;
        return flag;
    }

    public String getBlockMeta(int i) {
        String s;
        if(m_blockMeta != null && i >= 0 && i < m_blockMeta.length)
            s = m_blockMeta[i];
        else
            s = null;
        return s;
    }

    public String getFileMeta() {
        return m_fileMeta;
    }

    public int getFileSize() {
        return m_dataSize;
    }

    public String getFileStoreID() {
        return m_fileStoreID;
    }

    public String getNodeIP(int i) {
        return m_nodeIPs[0];
    }

    public int getNodeIPCount() {
        return m_nodeIPs.length;
    }

    public String getProtocol() {
        return m_protocolsStr;
    }

    public cn.kuaipan.kss.KssUpload.RequestUploadInfo.RequestUploadState getRequestUploadState() {
        return m_uploadState;
    }

    public cn.kuaipan.kss.KssDef.KssAPIResult getResult() {
        return cn.kuaipan.kss.KssDef.KssAPIResult.OK;
    }

    public byte[] getSeureKey() {
        return m_secureKey;
    }

    public boolean parseRequestUploadInfo(String s) {
        _clear();
        JSONObject jsonobject = new JSONObject(s);
        boolean flag = parseInfo(jsonobject);
_L2:
        return flag;
        Exception exception;
        exception;
        System.out.println(exception.toString());
        flag = false;
        if(true) goto _L2; else goto _L1
_L1:
    }

    public boolean parseRequestUploadInfo(JSONObject jsonobject) {
        boolean flag;
        if(jsonobject == null)
            flag = false;
        else
            flag = parseInfo(jsonobject);
        return flag;
    }

    private boolean m_blockIsExist[];
    private String m_blockMeta[];
    private int m_dataSize;
    private String m_fileMeta;
    private String m_fileStoreID;
    private String m_nodeIPs[];
    private String m_protocolsStr;
    private byte m_secureKey[];
    private cn.kuaipan.kss.KssUpload.RequestUploadInfo.RequestUploadState m_uploadState;
}
