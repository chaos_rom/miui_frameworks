// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package cn.kuaipan.kss.appmaster;

import cn.kuaipan.kss.utils.Encode;
import org.json.JSONArray;
import org.json.JSONObject;

class _BlockInfo
    implements cn.kuaipan.kss.KssDownload.BlockDownloadInfo {

    public _BlockInfo(int i) {
        m_blockIndex = -1;
        m_blockSize = 0;
        m_fullURL = null;
        m_blockSHA1 = null;
        m_blockIndex = i;
    }

    public String getBlockDownload_FullURL(int i) {
        return m_fullURL;
    }

    public int getBlockIndex() {
        return m_blockIndex;
    }

    public byte[] getBlockSHA1() {
        return m_blockSHA1;
    }

    public int getBlockSize() {
        return m_blockSize;
    }

    public int getDownloadURLCount() {
        int i;
        if(m_fullURL == null || m_fullURL.equals(""))
            i = 0;
        else
            i = 1;
        return i;
    }

    public boolean parseBlockInfo(JSONObject jsonobject) {
        boolean flag = false;
        if(jsonobject != null) goto _L2; else goto _L1
_L1:
        return flag;
_L2:
        m_blockSHA1 = Encode.hexStringToByteArray(jsonobject.getString("sha1"));
        m_fullURL = jsonobject.getJSONArray("urls").getString(0);
        m_blockSize = jsonobject.getInt("size");
        flag = true;
        continue; /* Loop/switch isn't completed */
        Exception exception;
        exception;
        exception.printStackTrace();
        m_fullURL = "";
        m_blockSize = 0;
        if(true) goto _L1; else goto _L3
_L3:
    }

    private int m_blockIndex;
    private byte m_blockSHA1[];
    private int m_blockSize;
    private String m_fullURL;
}
