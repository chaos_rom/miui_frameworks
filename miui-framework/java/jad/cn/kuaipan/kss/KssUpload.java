// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) nonlb 

package cn.kuaipan.kss;

import java.io.InputStream;

public interface KssUpload {
    public static interface UploadTransControl {
    }

    public static interface UploadResult {

        public abstract String getBlockCommitMeta(int i);

        public abstract int getBlockCount();

        public abstract String getFileMeta();

        public abstract KssDef.KssAPIResult getResult();
    }

    public static interface RequestUploadInfo {
        public static final class RequestUploadState extends Enum {

            public static RequestUploadState valueOf(String s) {
                return (RequestUploadState)Enum.valueOf(cn/kuaipan/kss/KssUpload$RequestUploadInfo$RequestUploadState, s);
            }

            public static RequestUploadState[] values() {
                return (RequestUploadState[])$VALUES.clone();
            }

            private static final RequestUploadState $VALUES[];
            public static final RequestUploadState FileExisted;
            public static final RequestUploadState NeedUpload;

            static  {
                NeedUpload = new RequestUploadState("NeedUpload", 0);
                FileExisted = new RequestUploadState("FileExisted", 1);
                RequestUploadState arequestuploadstate[] = new RequestUploadState[2];
                arequestuploadstate[0] = NeedUpload;
                arequestuploadstate[1] = FileExisted;
                $VALUES = arequestuploadstate;
            }

            private RequestUploadState(String s, int i) {
                super(s, i);
            }
        }


        public abstract int getBlockCount();

        public abstract boolean getBlockIsExist(int i);

        public abstract String getBlockMeta(int i);

        public abstract String getFileMeta();

        public abstract int getFileSize();

        public abstract String getFileStoreID();

        public abstract String getNodeIP(int i);

        public abstract int getNodeIPCount();

        public abstract String getProtocol();

        public abstract RequestUploadState getRequestUploadState();

        public abstract KssDef.KssAPIResult getResult();

        public abstract byte[] getSeureKey();
    }


    public abstract UploadResult getCommitInfo() throws Exception;

    public abstract boolean init(RequestUploadInfo requestuploadinfo, KssDef.OnUpDownload onupdownload, KssDef.NetState netstate);

    public abstract void terminal();

    public abstract KssDef.KssAPIResult upload(InputStream inputstream) throws Exception;

    public abstract KssDef.KssAPIResult upload(byte abyte0[]) throws Exception;
}
